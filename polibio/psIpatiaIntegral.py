import numpy as np
import scipy.special
from timeit import default_timer as timer
from math import sqrt, log

## rad1 = A*(B-d)**-n1
## rad1 = A'*(B'+d)**-n2


#>> integrate( (B-d)**(-n),d)
#-Piecewise((log(B - d), Eq(-n, -1)), ((B - d)**(-n + 1)/(-n + 1), True))
#>


def int_R(d0,d1, A,B,n):
    if d0 == d1: return 0
    if n!= 1: return A*((B + d1)**(-n + 1)/(-n + 1) - (B + d0)**(-n + 1)/(-n + 1))
    return A*(log((B + d1)/(B+d0)))
def int_L(d0,d1, A,B,n):
    if d0 == d1: return 0
    if n!= 1: return -A*((B - d1)**(-n + 1)/(-n + 1) -  (B - d0)**(-n + 1)/(-n + 1))
    return -A*(log((B - d1)/(B-d0)))

def integral(x0,x1, mu,sigma,l,a1, n1, a2, n2):
    dmin = x0 - mu
    dmax = x1 - mu
    
    delta = sigma *sqrt(-2 *(1+l))
    da1 = -a1*sigma
    da2 = a2*sigma
    d0 = max(dmin, da1)
    d1 = min(dmax, da2)
    delta2 = delta*delta

    ## Core
    S_core = -d0*scipy.special.hyp2f1(0.5, -1.0*l + 0.5, 1.5, -1.0*d0**2/delta2) + d1* scipy.special.hyp2f1(0.5, -1.0*l + 0.5, 1.5, -1.0*d1**2/delta2)

    ## Tail left
    phi = 1. + da1*da1/delta2
    k1 = phi**(l-0.5)
    k2 = (l-0.5)*phi**(l-1.5)*2*da1/delta2
    B1 = da1 + n1*k1/k2
    A1 = k1*(B1-da1)**n1
    S_left = int_L(dmin, d0, A1, B1, n1)

    ## Tail right
    phi = 1. +  da2*da2/delta2
    k1 = phi**(l-0.5)
    k2 = (l-0.5)*phi**(l-1.5)*2.*da2/delta2
    B2 = -da2 - n2*k1/k2
    A2 = k1*(B2 + da2)**n2
    S_right = int_R(d1,dmax, A2,B2,n2)
   
    return  S_left + S_core + S_right
    
