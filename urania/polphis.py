from Helicity import *
from LatexFunctions import *
from os import system
A = doB2VX([0,1], helicities = [1,-1], transAng = 0)
pdf_split = DecomposeAmplitudes(A,TransAmplitudes.values())#H.values())
phys = 0
gplus = Symbol("gplus")
gminus = Symbol("gminus")
WeakPhases = {}
Lambdas = {}
#{'1_pa': A_pa, '1_pe': A_pe, '0_0': A_S, '1_0': A_0}

    
#free_delta0 = 1
def DefineWeakPhases(free_delta0 = 0):
    for key in TransAmplitudes:
        amp = str(TransAmplitudes[key])
        #name = str(amp)
        if key not in Lambdas.keys(): Lambdas[key] = Symbol("\\|\\lambda\\|" + amp.replace("A",""),positive = True)
        if key not in WeakPhases.keys(): WeakPhases[key] = Symbol("\\Phi" + amp.replace("A",""),real = True)
    #if not free_delta0: TransAmpPhases.pop("1_0")

free_delta0 = 1
DefineStrongPhases(free_delta0)
DefineWeakPhases(free_delta0)
#### Rotate CP = -1 phases for consistency with Note's notation
WeakPhases["0_0"] += Pi 
WeakPhases["1_pe"] += Pi


modlist = []
itsamplitudes = []
texamplitudes = []
for key in TransAmplitudes.keys():
    if key == "1_0" and not free_delta0:
        modlist.append( (TransAmplitudes[key], TransAmpModuli[key]))
        continue
    modlist.append( (TransAmplitudes[key],TransAmpModuli[key]*Exp(I*TransAmpPhases[key])*(gplus +Lambdas[key]*Exp(-I*WeakPhases[key])*gminus) ))
    itsamplitudes.append( (TransAmplitudes[key],TransAmpModuli[key]))
    texamplitudes.append( (TransAmplitudes[key],Abs(TransAmplitudes[key])))

def getThing(A,thing):
    return simplify(A.collect(thing,evaluate = False) [thing])
def massageTimeEvolution(A):
    th1= getThing(A,gplus*gplus.conjugate())
    #print "(0-0)"
    #print th1
    th2= getThing(A,gminus*gminus.conjugate())
    #print "-_-"
    #print th2
    th3= getThing(A,gplus*gminus.conjugate())
    #print "(-)"
     #print th3
    th4= getThing(A,gminus*gplus.conjugate())
    #print "=_="
    #print th4
    return th1*gplus*gplus.conjugate() + th2*gminus*gminus.conjugate() + th3*gplus*gminus.conjugate() + th4*gminus*gplus.conjugate()
def doTimeEvolution(expr):    
    f = expr.func
    if f in [im,re]:
        arg = expr.args[0]
        arg = arg.subs( modlist )
        arg = arg.rewrite(Exp,Cos)
        #return f(arg)
        #print f(arg).expand()
        return massageTimeEvolution(arg.expand())
        #return simplify(f(arg))
    elif expr.is_Pow and expr.args[0].func == Abs:
        xx = expr.args[0].args[0]
        expr2 = xx*xx.conjugate()
        print "xx"
        print xx
        out = expr2.subs( modlist )
        out = out.rewrite(Exp,Cos)
        print "This is how it looks like before massage and expand:"
        print out.expand()
        return simplify(massageTimeEvolution(out.expand()))
    else: cancer
    #return simplify(out)

#a = doTimeEvolution(pdf_split.keys()[0])
#beta =  a.args[0]
#z  =massageTimeEvolution(beta.expand())
print " Adding time evolution\n -------------"
f = file("urania_phis_b.tex","w")
def begintex(f):
    """ adds some lines at the startup to make a tex file
    """
    f.write("\\documentclass{article}\n")
    f.write("\\usepackage[paperwidth=200in, paperheight=8.5in]{geometry}\n")
    f.write("\\pdfoutput=1\n")
    #f.write("\\usepackage{jheppub}\n")
    f.write("\\usepackage{graphicx}\n")
    f.write("\\usepackage{amsbsy}\n")
    f.write("\\usepackage{amsfonts}\n")
    f.write("\\usepackage{amsmath}\n")
    #f.write("\\usepackage{amssymb}]\n")
    f.write("\\usepackage{wasysym}\n")
    f.write("\\usepackage{multirow}\n")
    f.write("\\usepackage{psfrag}\n")
    f.write("\\usepackage{url}\n")
    f.write("\\title{Thing}\n")
    f.write("\\renewcommand{\\thefootnote}{\\arabic{footnote}}\n")
    f.write("\\setcounter{footnote}{0}\n")
    f.write("\\vspace{0.6cm}\n")
    #f.write("\\abstract{.}\n")
    f.write("\\begin{document}\n")


begintex(f)
#begin_multline(f)
f.write("\\begin{table}[h]\n")
f.write("\\begin{center}\n")
f.write("\\begin{tabular}{c|c|c|c|c|c|}\n")
f.write("Amp& $f(\Omega)$ & $a_k$ & $b_k$ & $c_k$ &$ d_k$ \\\\" + "\n")
f.write("\hline\\\\\n")
i = 0
def CustomTrigSimp1(expr):
    out = expr
    phases = TransAmpPhases.values() + WeakPhases.values()
    for i in range(len(phases)):
        delta = phases[i]
        for j in range(i+1,len(phases)):
            phi = phases[j]
            out = out.subs([(Sin(delta)*Cos(phi),Sin(delta- phi) + Cos(delta)*Sin(phi)), (Sin(delta)*Sin(phi),Cos(delta- phi) - Cos(delta)*Cos(phi))])
            out2 = simplify(out.subs([(Sin(phi)*Cos(delta),Sin(phi+ delta) - Cos(phi)*Sin(delta)), (Sin(phi)*Sin(delta),-Cos(phi+ delta) + Cos(phi)*Cos(delta))]))
            out = simplify(out)
                   
            if len(str(out2)) < len(str(out)): out = out2 
    return out

def CustomTrigSimp2(expr):
    out = expr
    phases = TransAmpPhases.values() + WeakPhases.values()
    for i in range(len(phases)):
        delta = phases[i]
        for j in range(i+1,len(phases)):
            phi = phases[j]
            out = out.subs([(Sin(delta)*Cos(phi),Sin(delta- phi) + Cos(delta)*Sin(phi)), (Cos(delta)*Cos(phi),Cos(delta- phi) - Sin(delta)*Sin(phi))])
            out2 = simplify(out.subs([(Sin(phi)*Cos(delta),Sin(phi+ delta) - Cos(phi)*Sin(delta)), (Cos(phi)*Cos(delta),Cos(phi+ delta) + Sin(phi)*Sin(delta))]))
            out = simplify(out)
            if len(str(out2)) < len(str(out)): out = out2 
    return out



## def CustomTrigSimp1(expr):
##     out = expr
##     phases = TransAmpPhases.values() + WeakPhases.values()
##     for i in range(len(phases)):
##         delta = phases[i]
##         for j in range(i+1,len(phases)):
##             phi = phases[j]
##             out = out.subs([(Sin(delta)*Cos(phi),Sin(delta- phi) + Cos(delta)*Sin(phi)), (Sin(delta)*Sin(phi),Cos(delta- phi) - Cos(delta)*Cos(phi))])
##             out = out.subs([(Sin(phi)*Cos(delta),Sin(phi+ delta) - Cos(phi)*Sin(delta)), (Sin(phi)*Sin(delta),-Cos(phi+ delta) + Cos(phi)*Cos(delta))])
##     return simplify(out)

def CustomTrigSimp(expr):
    l = []
    out1 = CustomTrigSimp1(expr)
    l.append([len(str(out1)),out1])
    out2 = CustomTrigSimp2(expr)
    l.append([len(str(out2)),out2])
    l.sort()
    return l[0][1]#.subs([(WeakPhases["0_0"], WeakPhases["0_0"] + Pi),(WeakPhases["1_pe"], WeakPhases["1_pe"] + Pi)

for key in pdf_split:
    if i > 20:
            i = 0
            multline_break(f)

   # print key
    t_dep = doTimeEvolution(key)
    if key.func.is_Pow:
        amps = key.args[0].subs(itsamplitudes)**2
        ampsx = key.args[0].subs(texamplitudes)**2
    else:
        amps = key.args[0].subs(itsamplitudes)
        ampsx = key.args[0].subs(texamplitudes)
    print amps
    if key.func in [re,im]:
        print "Here"
        _func = key.func
        t_dep = t_dep.expand().collect(amps,evaluate = False) [amps]
        alpha = getThing(t_dep,gplus*gplus.conjugate())
        beta = getThing(t_dep,gminus*gminus.conjugate())
        alphap = getThing(t_dep,gminus*gplus.conjugate())
        betap = getThing(t_dep,gplus*gminus.conjugate())
        ak = CustomTrigSimp(_func(alpha + beta))
        ck = CustomTrigSimp(_func(alpha - beta))
        bk = CustomTrigSimp(-_func((alphap + betap)))
        dk = CustomTrigSimp(_func((alphap-betap)*I))
    else:
        print "There"
        print t_dep.func
        t_dep = t_dep.expand().collect(amps,evaluate = False) [amps]
        alpha = getThing(t_dep,gplus*gplus.conjugate())
        beta = getThing(t_dep,gminus*gminus.conjugate())
        alphap = getThing(t_dep,gminus*gplus.conjugate())
        betap = getThing(t_dep,gplus*gminus.conjugate())
        ak = CustomTrigSimp(alpha + beta)
        ck = CustomTrigSimp(alpha - beta)
        bk = CustomTrigSimp(-(alphap + betap))
        dk = CustomTrigSimp((alphap-betap)*I)    
    #BREAK
    if pdf_split[key]: f.write("$" + latex(ampsx) + "$&$" + latex(pdf_split[key]) + "$&$" + latex(ak) + "$&$" + latex(bk) + "$&$" + latex(ck) + "$&$" + latex(dk)+ "$\\\\" + "\n")
    i += 1
    print "=)"
#end_multline(f)
f.write("\\end{tabular}\n")
f.write("\\end{center}\n")
f.write("\\end{table}\n")
f.write("\\end{document}\n")
f.close()
system("pdflatex " + "urania_phis_b")

