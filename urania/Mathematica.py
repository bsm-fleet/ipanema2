from sympy.printing.printer import Printer
from sympy.printing.precedence import precedence
### From Sympy Tutorial
### Added support for __Add__, __Mult__ , pi, ifty, limits, integrals and a manager
from re import match
from sympy import sympify
import os
from scipy import random as rnd

class MathematicaPrinter(Printer):
    """Print SymPy's expressions using Mathematica syntax. """
    printmethod = "_mathematica"

    _default_settings = {}

    _translation_table = {
        'asin': 'ArcSin',
        '_': 'u'
    }

    def parenthesize(self, item, level):
        printed = self._print(item)

        if precedence(item) <= level:
            return "(%s)" % printed
        else:
            return printed

    def emptyPrinter(self, expr):
        return str(expr)

    def _print_Pow(self, expr):
        prec = precedence(expr)

        if expr.exp == -1:
            return '1/%s' % (self.parenthesize(expr.base, prec))
        else:
            return '%s^%s' % (self.parenthesize(expr.base, prec),
                              self.parenthesize(expr.exp, prec))


    def _print_Symbol(self,expr):
        split_expr = expr.name.split("_")
        name = ""
        for arg in split_expr:
            name += arg+"underscore"

        return name[:-10]

    def _print_Function(self, expr):
        name = expr.func.__name__
        args = ", ".join([ self._print(arg) for arg in expr.args ])

        if expr.func.nargs is not None:
            try:
                name = self._translation_table[name]
            except KeyError:
                name = name.capitalize()

        return "%s[%s]" % (name, args)
    def _print_Mul(self, expr):
        prec = precedence(expr)
        return "*".join([ self.parenthesize(arg, prec) for arg in expr.args ])
    
    def _print_Add(self, expr):
        prec = precedence(expr)
        return "+".join([ self.parenthesize(arg, prec) for arg in expr.args ])
    
    def _print_Pi(self, expr): return 'Pi'
    def _print_Infinity(self,expr): return "Infinity"
    def _print_NegativeInfinity(self,expr): return "-Infinity"

    def __call__(self, expr): return self.doprint(expr)


## def mathematica(expr, **settings):
##     """Transform an expression to a string with Mathematica syntax. """
##     p = MathematicaPrinter(settings)
##     s = p.doprint(expr)

##     return s



def MathematicaToSympy(s):
    st = TranslateMathematica(s)
    st = st.replace("Sin","sin").replace("Cos","cos").replace("[","(").replace("]",")")
    st = st.replace("Cosh","cosh").replace("Sinh","sinh")
    return sympify(st)



def TranslateMathematica(s):
    """Translate Mathematica syntax to SymPy sintax. """
    s = s.strip()

    #Begin rules
    rules = (
        (r"\A(\w+)\[([^\]]+[^\[]*)\]\Z",  # Function call
        lambda m: translateFunction(m.group(1)) + "(" + TranslateMathematica(m.group(2)) + ")" ),

        (r"\((.+)\)\((.+)\)",  # Parenthesized implied multiplication
        lambda m: "(" + TranslateMathematica(m.group(1)) + ")*(" + TranslateMathematica(m.group(2)) + ")" ),

        (r"\((.+)\)\/\((.+)\)\Z",  # Parenthesized division
        lambda m: "(" + TranslateMathematica(m.group(1)) + ")/(" + TranslateMathematica(m.group(2)) + ")" ),


        (r"\A\((.+)\)\Z",  # Parenthesized expression
        lambda m: "(" + TranslateMathematica(m.group(1)) + ")" ),

        (r"\A(.*[\w\.])\((.+)\)\Z",  # Implied multiplication - a(b)
        lambda m: TranslateMathematica(m.group(1)) + "*(" + TranslateMathematica(m.group(2)) + ")" ),

        (r"\A\((.+)\)([\w\.].*)\Z",  # Implied multiplication - (a)b
        lambda m: "(" + TranslateMathematica(m.group(1)) + ")*" + TranslateMathematica(m.group(2)) ),

        (r"\A([\d\.]+)([a-zA-Z].*)\Z",  # Implied multiplicatin - 2a
        lambda m: TranslateMathematica(m.group(1)) + "*" + TranslateMathematica(m.group(2)) ),

        (r"\A([^=]+)(\*\^[\-\+]*)([^\]]+)\Z",  # Powers
         lambda m: TranslateMathematica(m.group(1)) + translateOperator(m.group(2)) + TranslateMathematica(m.group(3)) ),

        (r"\A([^=]+)([\^\-\*/\+=]=?)([^\]]+)\Z",  # Infix operator
        lambda m: TranslateMathematica(m.group(1)) + translateOperator(m.group(2)) + TranslateMathematica(m.group(3)) ),

        (r"(.+)\[([^\]]+[^\[]*)\](\)?)\Z", actionCompFunction))
    #End rules

    for rule, action in rules:
        m = match(rule, s)
        if m:
            return action(m)


    s_ = s.replace("underscore","_")
    s_ = s_.replace("Pi","pi")
    return s_


def actionCompFunction(m):
    
    prev = m.group(1)
    inside = m.group(2)
    after = ""
    if len(m.groups())>2: after = m.group(3)
    
    m2 = match(r"\A([^=]+)([\^\-\*/\+=]=?)([^\]]+)\Z",prev)

    if not m2:
        print prev
        print "Translation from Mathematica failed..."
        return "zero"
    
    return TranslateMathematica(m2.group(1))+translateOperator(m2.group(2))+translateFunction(m2.group(3))+"("+TranslateMathematica(inside)+")"+TranslateMathematica(after)


# def actionParenthesizedDivision(m):
#     num = m.group(1)
#     denom = m.group(2)
    
#     if len(m.groups())>2: denom = denom+"^"+m.group(3)

#     return "("+TranslateMathematica(num) + ")/(" + TranslateMathematica(denom) + ")" 

    
def translateFunction(s):
    if s.startswith("Arc"):
        return "a" + s[3:]
    return s.lower()


def translateOperator(s):
    dictionary = {'^': '**','*^':'e','*^-':'e-','*^+':'e+'}
    if s in dictionary:
        return dictionary[s]
    return s


def mathematica_script(filename):
    if os.path.exists("/afs/cern.ch/"): path = "/afs/cern.ch/project/parc/math90/bin/math"
    else: path = "math"
    os.system(path + " < " + filename);

def math_assumptions(func, args):
    mp = MathematicaPrinter()
    assump = "Assumptions->"
    for thing in func.atoms():
        if thing.is_real: assump += "Im["+mp(thing) +"]==0&&"
        if thing.is_positive: assump += mp(thing) +">0&&"

    for arg in args:
        for thing in arg:
            if thing.is_real: assump += "Im["+mp(thing) +"]==0&&"
            if thing.is_positive: assump += mp(thing)+">0&&"

    assump+= "}"
    assump = assump.replace("&&}","")
    return assump

def math_integrate(func, *args):

    mp = MathematicaPrinter()
    args_math = ""
    
    for arg in args:
        var = arg[0]
        lo = arg[1]
        hi = arg[2]
        args_math += "{"+mp(var)+","+mp(lo)+","+mp(hi)+"}"+","

    func_math = mp(func)
    assump = math_assumptions(func, args)
    
    ftmo = open("int_tmp.nb","w")
    #ftmo.write("Put[FullSimplify[Integrate["+func_math+","+args_math[:-1]+"," + assump +"]],\"int_tmp.txt\"]\n")
    ftmo.write("Put[Simplify[Integrate["+func_math+","+args_math[:-1]+"," + assump +"]],\"int_tmp.txt\"]\n") 
    #else: ftmo.write("Put[Integrate["+func_math+","+args_math[:-1]+"],\"int_tmp.txt\"]\n")
    ftmo.close()

    mathematica_script("int_tmp.nb");

    if not os.path.isfile("int_tmp.txt"):
        print "Mathematica failed executing. Returning zero"
        return "zero"
    
    ftin = open("int_tmp.txt","r")
    line = ""
    while True:
        line_ = ftin.readline()
        if not line_: break
        line += line_[:-1]

    if not line:
        print "Something weird happened... Returning zero"
        return "zero"

    if "Integrate" in line:
        print "Mathematica failed calculating integral. Returning zero."
        return "zero"

#     os.remove("int_tmp.nb")
#     os.remove("int_tmp.txt")

    return MathematicaToSympy(line)
def math_limit(func, *args):

    mp = MathematicaPrinter()
    args_math = ""
    
    for arg in args:
        var = arg[0]
        var_lim = arg[1]
        args_math += mp(var)+"->"+mp(var_lim)

    func_math = mp(func)
    
    ftmo = open("int_tmp.nb","w")
    ftmo.write("Put[FullSimplify[Limit["+func_math+","+args_math+"]],\"int_tmp.txt\"]\n")
    ftmo.close()

    mathematica_script("int_tmp.nb");

    if not os.path.isfile("int_tmp.txt"):
        print "Mathematica failed executing. Returning zero"
        return "zero"
    
    ftin = open("int_tmp.txt","r")
    line = ""
    while True:
        line_ = ftin.readline()
        if not line_: break
        line += line_[:-1]
    if not line:
        print "Something weird happened... Returning zero"
        return "zero"

    if "Limit" in line:
        print "Mathematica failed calculating limit. Returning zero."
        return "zero"

#     os.remove("int_tmp.nb")
#     os.remove("int_tmp.txt")
    line = line.replace("/ ", "/")
    return re(MathematicaToSympy(line))

def math_fullintegrate(func, *args):

    mp = MathematicaPrinter()
    args_math = ""
    
    for arg in args:
        var = arg[0]
        lo = arg[1]
        hi = arg[2]
        args_math += "{"+mp(var)+","+mp(lo)+","+mp(hi)+"}"+","

    func_math = mp(func)
    assump = math_assumptions(func, args)

    
    ftmo = open("int_bbar.nb","w")
    ftmo.write("Put[FullSimplify[Integrate["+func_math+","+args_math[:-1]+"," + assump +"]],\"int_bbar.txt\"]\n")
    #ftmo.write("Put[Simplify[Integrate["+func_math+","+args_math[:-1]+"," + assump +"]],\"int_bbar.txt\"]\n") 
    #else: ftmo.write("Put[Integrate["+func_math+","+args_math[:-1]+"],\"int_bbar.txt\"]\n")
    ftmo.close()

    mathematica_script("int_bbar.nb");

    if not os.path.isfile("int_bbar.txt"):
        print "Mathematica failed executing. Returning zero"
        return "zero"
    
    ftin = open("int_bbar.txt","r")
    line = ""
    while True:
        line_ = ftin.readline()
        if not line_: break
        line += line_[:-1]

    if not line:
        print "Something weird happened... Returning zero"
        return "zero"

    if "Integrate" in line:
        print "Mathematica failed calculating integral. Returning zero."
        return "zero"
    return MathematicaToSympy(line)


class MathematicaManager:
    def __init__(self,junk):
        self.sympythings = {}
        for thing in junk:
            if 'name' in dir(thing):
                self.sympythings[thing.name] = thing
        self.SetFileName("crap"+str(rnd.random()))
        
        self.mp = MathematicaPrinter()
        self.simplevel = 1
        
    def SetFileName(self, fname):
        self.nb = fname + ".nb"
        self.txt = fname + ".txt"
        
    def SetSimplificationLevel(self, level):
        if not "lower" in dir(level): self.simplevel = level
        elif level.lower() == "full": self.simplevel = 2
        elif level.lower() == "simplify": self.simplevel = 1
        elif level.lower() == "none": self.simplevel = 0
        
    def resurrect(self, expr):
        mysubs = []
        for thing in expr.atoms():
            if 'name' in dir(thing):
                if thing.name in self.sympythings.keys(): mysubs.append((thing,self.sympythings[thing.name]))
        return expr.subs(mysubs)

    def integrate(self, func, *args):
        mp = self.mp
        args_math = ""
        
        for arg in args:
            var = arg[0]
            lo = arg[1]
            hi = arg[2]
            args_math += "{"+mp(var)+","+mp(lo)+","+mp(hi)+"}"+","

        func_math = mp(func)
        assump = math_assumptions(func, args)
        
    
        ftmo = open(self.nb,"w")
        
        ftmo.write(self.CommandString("Integrate["+func_math+","+args_math[:-1]+"," + assump +"]"))
                   
        ftmo.close()


        return self.Return()

    def CommandString(self, cmd):
        op = "Put[" + "FullSimplify[" * (self.simplevel == 2) + "Simplify[" * (self.simplevel == 1)
        clos = "]"*(self.simplevel != 0)
        return op + cmd + clos +",\"" +self.txt+"\"]\n"

    def Return(self):
        mathematica_script(self.nb)

        if not os.path.isfile(self.txt):
            print "Mathematica failed executing. Returning zero"
            return "zero"
    
        ftin = open(self.txt,"r")
        line = ""
        while True:
            line_ = ftin.readline()
            if not line_: break
            line += line_[:-1]

        if not line:
            print "Something weird happened... Returning zero"
            return "zero"

        if "Integrate" in line:
            print "Mathematica failed calculating integral. Returning zero."
            return "zero"

        if "Limit" in line:
            print "Mathematica failed calculating limit. Returning zero."
            return "zero"
        #os.system("rm " + self.nb)
        #os.system("rm " + self.txt)
        
        return self.resurrect(MathematicaToSympy(line))        
    
    def limit(self, func, *args):

        mp = self.mp 
        args_math = ""
        
        for arg in args:
            var = arg[0]
            var_lim = arg[1]
            args_math += mp(var)+"->"+mp(var_lim)

        func_math = mp(func)
        assump = math_assumptions(func, args)
    
        ftmo = open(self.nb,"w")
        ftmo.write(self.CommandString("Limit["+func_math+","+args_math+"," + assump +"]"))
        ftmo.close()


        return self.Return()
