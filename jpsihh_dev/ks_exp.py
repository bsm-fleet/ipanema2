import numpy as np
from scipy import random as rnd
from scipy.special import erfinv
from tools import initialize
import pycuda
import pycuda.gpuarray as gpuarray
from iminuit import Minuit
from math import sqrt
from ROOT import TMath
from bisect import *
initialize(0)

def integralExp(k, tmin, tmax):
    if k!= 0 :  return (np.exp(k*tmax)-np.exp(k*tmin))*1./k 
    return(tmax - tmin)

t = -np.log(rnd.random(10000))
t.sort()
t_gpu = gpuarray.to_gpu(t)
tsum = np.sum(t)

tmin = 0
tmax = 15.
Ntot = len(t)
G = -2
expo_int = pycuda.cumath.exp(G*t_gpu)- np.exp(G*tmin)
expo_int *= 1./(np.exp(G*tmax)-np.exp(G*tmin))


def FCN(k):
    
    ws = np.exp(k*t) ## simple case, exponential acceptance exp(-k*t) --> w = 1/eff
    cws =  gpuarray.to_gpu(np.cumsum(ws))
    N0 = np.float64(cws[-1].get())
    cws *= 1./N0
    D = cws - expo_int
    Dmax= np.float64(gpuarray.max(pycuda.cumath.fabs(D)).get())
    KSstat = sqrt(Ntot)*Dmax
    Prob = TMath.KolmogorovProb(KSstat)
    print Prob
    if Prob < 1e-7: chi2 = 25 + KSstat 
    else: chi2 = 2*erfinv(1-Prob)**2

    return chi2

cws0 = np.float64(range(len(t)))
N0 = cws0[-1]
cws0 *= 1./N0


def FCN2(k):
    if k!=0:
        m_expo_int = np.exp(k*t)- np.exp(k*tmin)
        m_expo_int *= 1./(np.exp(k*tmax)-np.exp(k*tmin))
    else:
        m_expo_int = t - tmin
        m_expo_int *= 1./(tmax - tmin)
    D = cws0 - m_expo_int
    Dmax= max(np.abs(D))
    KSstat = sqrt(Ntot)*Dmax
    Prob = TMath.KolmogorovProb(KSstat)
    #print Dmax, Prob
    if Prob < 1e-7: chi2 = 25 + KSstat 
    else: chi2 = 2*erfinv(1-Prob)**2

    return chi2

def FCN3(k):
    if k!=0:
        integral = (np.exp(k*tmax)-np.exp(k*tmin))*1./k
    else:
        integral = (tmax - tmin)
    LL = k*tsum - np.log(integral)*Ntot 

    return -2*LL
points = np.arange(-1.5,0,.001)
chi2l = []
for point in points:
    chi2l.append([FCN2(point), point])
chi2l.sort()
chi2l = np.float64(chi2l)
xbest =  chi2l[:,1][0]
chmin = chi2l[:,0][0]

i = bisect(chi2l[:,0],chmin + 1)

fit = Minuit(FCN3, limit_k = (-1.5,0))
fit.migrad()
print "KS fit:" , xbest, abs(chi2l[:,1][i] - xbest)
