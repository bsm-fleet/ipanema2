from tools import initialize
import sys
MODE = 0#int(sys.argv[1])
initialize(MODE)
from bisect import *
from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
from iminuit import Minuit
import pycuda.gpuarray as gpuarray
import cPickle
from math import sqrt, isnan
from ROOT import TMath
from scipy import random as rnd
from scipy.special import erfinv
#from multiprocessing import Pool
from matplotlib import pyplot as plt
Gs = 0.6614 
DGs = 0.08543 
Gd = 1./ 1.519
Gddat = 1./1.520
Gudat = 1./1.638
Gu = 1./1.638
sf = 5367./5280
SAMPLE = "Bu" + "dat"*MODE
#SW = "cor_sWeights_Bd"
#SW = "truth_match"
sBIN = "1"#sys.argv[2]
BIN = int(sBIN)
if "dat" in SAMPLE: SW = "cor_sWeights_Bd"
k_TRUE = {"Bd":-Gd, "Bs": - Gs , "Bu": -Gu , "Bddat": -Gddat, "Budat": -Gudat}
if SAMPLE == "Bu": data = cPickle.load(file("/scratch28/diego/b2cc/Bu_MC_bin" + sBIN + "cor_sWeights_Bd_0.3ps.crap"))
elif SAMPLE == "Budat": data = cPickle.load(file("/scratch28/diego/b2cc/Bu_dat_bin" + sBIN + "cor_sWeights_Bd_0.3ps.crap"))

tmin = .3
tmax = 15.
PTbins = [0, 3.8, 6, 9, 100]
PTmin = 1000*PTbins[BIN-1]
PTmax = 1000*PTbins[BIN]
data.sort()
data = np.float64(data)

#BREAK
mask = (( data[:,0] > tmin ) * ( data[:,0] < tmax)*( data[:,6] > PTmin ) *( data[:,6] < PTmax ) )
t_gpu = gpuarray.to_gpu(np.extract(mask,data[:,0]))
mup_DZ_gpu = gpuarray.to_gpu(np.extract(mask, data[:,1]))
mum_DZ_gpu = gpuarray.to_gpu(np.extract(mask,data[:,2]))
hp_DZ_gpu = gpuarray.to_gpu(np.extract(mask,data[:,3]))
hm_DZ_gpu = gpuarray.to_gpu(np.extract(mask, data[:,4]))


G = k_TRUE[SAMPLE]
sw_gpu = gpuarray.to_gpu(np.extract(mask, data[:,5]))
expo_int = pycuda.cumath.exp(G*t_gpu)- np.exp(G*tmin)
expo_int *= 1./(np.exp(G*tmax)-np.exp(G*tmin))

Ntot = sum(mask)


def FCN(a, epsmu):#, epshm):
    shit = a*t_gpu*t_gpu*t_gpu
    eff = (1. + epsmu*mup_DZ_gpu*mup_DZ_gpu) * (1. + epsmu*mum_DZ_gpu*mum_DZ_gpu)* (1. + epsmu*hp_DZ_gpu*hp_DZ_gpu)* (1. + epsmu*hm_DZ_gpu*hm_DZ_gpu)
    eff*= shit/(1 + shit)
    #if isnan(epsmu) : return 1.e8 + rnd.random()*1e9
    #if isnan(epsh) : return 1.e8 + rnd.random()*1e9
    
    ws = 1./eff*sw_gpu
    cws =  gpuarray.to_gpu(np.cumsum(ws.get()))
    N0 = np.float64(cws[-1].get())
    cws *= 1./N0
    D = cws - expo_int
    Dmax= np.float64(gpuarray.max(pycuda.cumath.fabs(D)).get())
    KSstat = sqrt(Ntot)*Dmax
    Prob = TMath.KolmogorovProb(KSstat)
    #print "1:", epsmu,epsh
    if Prob < 1e-7: chi2 = 25 + KSstat 
    else: chi2 = 2*erfinv(1-Prob)**2
    #print "2:", Prob, KSstat, chi2

    return chi2

#BREAK
fit = Minuit(FCN, epsmu = -4e-03, fix_epsmu = True, limit_a = (1e-03, 1000))
fit.migrad()
#BREAK
def contFCN(pair): return FCN(pair[0],pair[1])
aval, saval = fit.values["a"], fit.errors["a"]
delta = 1e-4
y = np.arange(-0.05, 0.05, delta)
x = np.arange(aval -30, aval+30, 1)

pairs = []
for thing in x:
    for crap in y: pairs.append([thing,crap])

X,Y = np.meshgrid(x,y)


def makeZ(zlist):
    Z = X*0
    k = 0
    for i in range (len(x)):
        for j in range( len(y)) :
            Z[j][i] = zlist[k]
            k += 1
    return Z

start = timer()
#chi2list = map(contFCN, pairs)
#Z = makeZ(chi2list)

chi2list = map(contFCN,pairs)
Z = makeZ(chi2list)
print "time:" , timer() - start
chi2list = np.float64(chi2list)

mask_plot = chi2list < 9
x_plot = np.extract(mask_plot,x)
chi_plot =np.extract(mask_plot, chi2list)
plt.plot(x_plot,chi_plot)

shit = []
for i in range(len(chi2list)): shit.append([chi2list[i], pairs[i][0], pairs[i][1]])
shit.sort()
shit = np.float64(shit)
xmin = shit[0][1]
ymin = shit[0][2]
chmin = shit[0][0]
xp = np.extract( shit[:,1] > xmin, shit[:,1])
xp_ch = np.extract( shit[:,1] > xmin, shit[:,0])
xn = np.extract( shit[:,1] < xmin, shit[:,1])
xn_ch = np.extract( shit[:,1] < xmin, shit[:,0])

if chmin > 4: print "warning, abschi2 = :", chmin
print xmin, xn[bisect(xn_ch,chmin + 1)] - xmin, xp[bisect(xp_ch,chmin+ 1)] - xmin 
xp = np.extract( shit[:,2] > ymin, shit[:,2])
xp_ch = np.extract( shit[:,2] > ymin, shit[:,0])
xn = np.extract( shit[:,2] < ymin, shit[:,2])
xn_ch = np.extract( shit[:,2] < ymin, shit[:,0])
print ymin, xn[bisect(xn_ch,chmin + 1)] - ymin, xp[bisect(xp_ch,chmin+ 1)] - ymin
cs = plt.contour(X,Y,Z, levels = [chmin + 2*1.15, chmin+5.99,chmin+11.8 ])
plt.clabel(cs, inline=1, fontsize=10)
plt.xlabel("a")
plt.ylabel("eps_mu")

#|    0 |   eps = -0.002944 |  0.001083 |          |          |          |          |          |
#----------------------------------------------------------------------------------------------
#cs = plt.contour(X,Y,Z, levels = [min(chi2list) + 2*1.15, min(chi2list)+5.99,min(chi2list)+11.8 ]) ## 4 is 1.5 sigma

#plt.clabel(cs, inline=1, fontsize=10)
#plt.xlabel("eps_mu")
#plt.ylabel("eps_h")

#plt.show()


# 4ps
###
# MC
#-0.0074 -0.0037 0.0064
#-0.0082 -0.0021 0.0021
#-0.003 -0.0016 0.002
#-0.0053 -0.0004 0.001

# Data
#0.0041 -0.0115 0.0169
#-0.0025 -0.003 0.0027
#-0.0033 -0.0023 0.0024
#-0.006 -0.0009 0.0015

# 5ps
#MC
#0.0024 -0.0068 0.0111
#-0.0076 -0.0016 0.0049
#-0.0038 -0.0018 0.0017
#-0.0051 -0.0004 0.0006

# Data
#-0.0058 -0.0113 0.0154
#-0.0082 -0.0024 0.0042
#-0.0041 -0.0043 0.0022
#-0.0064 -0.0027 0.0012
