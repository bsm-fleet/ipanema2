from matplotlib import pyplot as plt
import cPickle
import numpy as np
delta = 1e-4

x = np.arange(-0.02, 0.02, delta)
y = np.arange(-0.02, 0.02, delta)
X,Y = np.meshgrid(x,y)
Z = cPickle.load(file("contour.crap"))
cs = plt.contour(X,Y,Z, levels = [2*1.15, 5.99,11.8 ]) ## 4 is 1.5 sigma

plt.clabel(cs, inline=1, fontsize=10)
plt.xlabel("eps_mu")
plt.ylabel("eps_h")

plt.show()
