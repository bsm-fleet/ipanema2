import numpy as np
import pycuda.autoinit
import pycuda.cumath
import pycuda.driver as cudriver

from pycuda.compiler import SourceModule
import psIpatia as ipatia_module
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import ParamBox, cuRead
from tools import getSumLL_w_large, getSumLL_w_short,getSumLL_large, getSumLL_short, plt, get_pulls, plot1D
from timeit import default_timer as timer
from phisParams import CSP
from poisson_intervals import *
from math import pi

mod = cuRead("cuda_b2cc_ang_acc.c") ## ML : changed this to account for the time acceptance
pdf = mod.get_function("DiffRate")

#_timebins = np.arange(0.3,14,13.7/10000)
#_timebins = np.arange(0.,14,14./10000) ## ML : changed this to match Chobi's desires
_timebins = np.arange(0.2,15,14.8/10000) 
_timebins_gpu = gpuarray.to_gpu(_timebins)
_pdf_time = _timebins_gpu.copy()*0

THR = 1e06

def mySum1(thing): 
    return np.float64(sum(thing))

def mySum2(thing): 
    return np.float64(sum(thing).get())

class Badjanak(ParamBox):
    def __init__(self, pars, cats, weighted = 1, blind = 1):
        ParamBox.__init__(self, pars, cats)
        sizes = []
        self.weighted = weighted
        
        for k in cats: 
            sizes.append(k.Nevts)
            
        if max(sizes) > THR:
            if weighted: 
                self.getSumLL = getSumLL_w_large
            else : 
                self.getSumLL = getSumLL_large
            self.mySum = mySum2
        else:
            if weighted: 
                self.getSumLL = getSumLL_w_short
            else: 
                self.getSumLL = getSumLL_short
            self.mySum = mySum1
            
        self.pdf = pdf
        self.time_bins = _timebins_gpu
        self.pdf_time = _pdf_time
        self.N_tbins = self.time_bins.size
        self.blind = blind

    def run_cat(self, cat, CSP, Fs, fL, fpe, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm): 
        As = np.sqrt(Fs)
        Fp = 1.-Fs
        A0 = np.sqrt(Fp*fL)
        Ape = np.sqrt(Fp*fpe)
        Apa = np.sqrt(Fp*(1-fL-fpe))
   
        self.pdf(cat.data, cat.Probs, CSP, A0, As, Apa, Ape, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm, cat.Nevts, block = cat.block,grid = cat.grid)
        
        if self.weighted: 
            return cat.wNorm  
        else: 
            return np.float64(cat.Nevts)
    
    def __call__(self,*args):
            
        chi2 = np.float64(0.)
        N = self.dc
            
        fL = np.float64(args[N["fL"]])
        fpe = np.float64(args[N["fpe"]])
        phis_0 = np.float64(args[N["phis_0"]] + self.blind*self.params[N["phis_0"]].BlindOffset())
        #phis_S = np.float64(args[N["phis_S"]] + self.blind*self.params[N["phis_S"]].BlindOffset())
        phis_S = np.float64(args[N["phis_0"]] + self.blind*self.params[N["phis_0"]].BlindOffset())
        phis_pa = np.float64(args[N["phis_0"]] + self.blind*self.params[N["phis_0"]].BlindOffset())
        #phis_pa = np.float64(args[N["phis_pa"]] + self.blind*self.params[N["phis_pa"]].BlindOffset())
        phis_pe = np.float64(args[N["phis_0"]] + self.blind*self.params[N["phis_0"]].BlindOffset())
        #phis_pe = np.float64(args[N["phis_pe"]] + self.blind*self.params[N["phis_pe"]].BlindOffset())
        dpa = np.float64(args[N["dpa"]])
        dpe = np.float64(args[N["dpe"]]+pi)
        lambda_0_abs = np.float64(args[N["lambda_0_abs"]] + self.blind*self.params[N["lambda_0_abs"]].BlindOffset())
        lambda_S_abs = np.float64(args[N["lambda_0_abs"]] + self.blind*self.params[N["lambda_0_abs"]].BlindOffset()) 
        #lambda_S_abs = np.float64(args[N["lambda_S_abs"]] + self.blind*self.params[N["lambda_S_abs"]].BlindOffset()) 
        lambda_pa_abs = np.float64(args[N["lambda_0_abs"]] + self.blind*self.params[N["lambda_0_abs"]].BlindOffset())
        #lambda_pa_abs = np.float64(args[N["lambda_pa_abs"]] + self.blind*self.params[N["lambda_pa_abs"]].BlindOffset())
        lambda_pe_abs = np.float64(args[N["lambda_0_abs"]] + self.blind*self.params[N["lambda_0_abs"]].BlindOffset())
        #lambda_pe_abs = np.float64(args[N["lambda_pe_abs"]] + self.blind*self.params[N["lambda_pa_abs"]].BlindOffset())
        G = np.float64(args[N["G"]])
        DG = np.float64(args[N["DG"]] + self.blind*self.params[N["DG"]].BlindOffset())
        Dm = np.float64(args[N["Dm"]])
         
        norm = np.float64(0.)

        for cat in self.cats:
            ibin = cat.ibin
            Fs = np.float64(args[N["Fs_" + ibin]])
            dS_m_dpe = np.float64(args[N["ds_m_dpe_" + ibin]]+args[N["dpe"]])
            CSP = np.float64(args[N["CSP_" + ibin]])
            norm +=  self.run_cat(cat, CSP, Fs, fL, fpe, phis_0, phis_S, phis_pa, phis_pe, dS_m_dpe, dpa, dpe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, G, DG, Dm)
            
        LL = map(self.getSumLL, self.cats)
        LLsum = self.mySum(LL)
        out = -2*LLsum + 2*norm
        out += ((Dm-17.768)/0.024)**2
        if math.isnan(out):
            return 10e12
        
        return out
    
    def plotcat(self,cat, obs = 3, rebinning = 50, outfile='test_fit.pdf', yscale='log', interval = poisson_Linterval, plotf = plot1D):
        #rebin                                                                                                                                                                                                      
       data = cat.data.get()[:,obs]  ## only the decaytime
       sigmat = cat.data.get()[:,4]  ## only sigmat
       q_OS = cat.data.get()[:,5]
       q_SSK = cat.data.get()[:,6]
       omega_OS = cat.data.get()[:,7]
       omega_SSK = cat.data.get()[:,8]
       Nsigmat = np.int32(len(sigmat))
       sigmat_gpu = gpuarray.to_gpu(sigmat)
       q_OS_gpu = gpuarray.to_gpu(q_OS)
       q_SSK_gpu = gpuarray.to_gpu(q_SSK)
       omega_OS_gpu = gpuarray.to_gpu(omega_OS)
       omega_SSK_gpu = gpuarray.to_gpu(omega_SSK)
       bins = self.time_bins.get()
       if obs != 3: 
           bins = np.arange(min(data),max(data), (max(data)-min(data))*1./100)
       #print bins
       dt = bins[1]-bins[0]
       bins2 = bins -np.float64(len(bins)*[-dt*0.5])
       bins2 = list(bins2)
       bins2.append(bins[-1]+ dt)
       bins2 = np.float64(bins2)
       datahist = np.histogram(data, bins2)[0]
       #print bins2, datahist
       N = self.fit.values
       #print N
       fL = np.float64(N["fL"])
       fpe = np.float64(N["fpe"])
       phis_0 = np.float64(N["phis_0"])
       phis_S = np.float64(N["phis_S"]) ## ML: changed this for the validation fit np.float64(N["phis_S"])
       #phis_S = np.float64([N["phis_0"]])
       #phis_pa = np.float64([N["phis_0"]])
       phis_pa = phis_0 ## ML : changed this for the validation fit np.float64([N["phis_pa"]])
       #phis_pe = np.float64([N["phis_0"]])
       phis_pe = phis_0 ## ML : changed this for the validation fit np.float64([N["phis_pe"]])
       dpa = np.float64(N["dpa"])
       dpe = np.float64(N["dpe"])
       lambda_0_abs = np.float64(N["lambda_0_abs"])
       lambda_S_abs = np.float64(N["lambda_S_abs"])
       lambda_pa_abs = np.float64(N["lambda_pa_abs"])
       lambda_pe_abs = np.float64(N["lambda_pe_abs"])

       G = np.float64(N["G"])
       DG = np.float64(N["DG"])
       Dm = np.float64(N["Dm"])
       
       ibin = cat.ibin
       Fs = np.float64(N["Fs_" + ibin])
       dS = np.float64(N["ds_m_dpe" + ibin] - N["dpe"])
       CSP = np.float64(N["CSP_" + ibin])
       As = np.sqrt(Fs)
       Fp = 1.-Fs
       A0 = np.sqrt(Fp*fL)
       Ape = np.sqrt(Fp*fpe)
       Apa = np.sqrt(Fp*(1-fL-fpe))
       self.pdf_time *= 0
       yblock = 1
       if obs == 3:
           #cat.integra(self.time_bins,self.pdf_time,sigmat_gpu, A0,As,Apa,Ape,G,DG,Dm,phis_0, phis_S, phis_pa, phis_pe, block = (10,yblock,1),grid = (self.N_tbins/10,int(len(sigmat)*1./yblock),1))
           cat.integra(self.time_bins, self.pdf_time, sigmat_gpu, q_OS_gpu, q_SSK_gpu, omega_OS_gpu, omega_SSK_gpu, A0, As, Apa, Ape, G, DG, Dm, phis_0, phis_S, phis_pa, phis_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, dS, dpa, dpe, CSP, Nsigmat, block = (1,1,1),grid = (self.N_tbins/1,1,1))#(10,1,1),grid = (self.N_tbins/10,1,1))

           pdf_ = self.pdf_time.get()
           integral = sum(pdf_)#*dt                                                                                                                                                                                    
           pdf = (len(data)*1./integral)*pdf_
       else: pdf = 0*datahist
       #print pdf
       fig , pltfit, pltpulls = plotf (datahist,pdf,bins,rebinning = rebinning, yscale = yscale, interval = interval)
       fig.tight_layout()
       fig.savefig(outfile)
       print Nsigmat
