from tools import initialize
initialize(0)
from bisect import *
from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
from iminuit import Minuit
import pycuda.gpuarray as gpuarray
import cPickle
from math import sqrt, isnan
from ROOT import TMath
from scipy import random as rnd
from scipy.special import erfinv
#from multiprocessing import Pool
from matplotlib import pyplot as plt
Gs = 0.6614 
DGs = 0.08543 
Gd = 1./ 1.519
Gddat = 1./1.520
Gudat = 1./1.638
Gu = 1./1.638
sf = 5367./5280
SAMPLE = "Bu"
#SW = "cor_sWeights_Bd"
SW = "truth_match"
if "dat" in SAMPLE: SW = "cor_sWeights_Bd"
k_TRUE = {"Bd":-Gd, "Bs": - Gs , "Bu": -Gu , "Bddat": -Gddat, "Budat": -Gudat}
if SAMPLE == "Bd": data = cPickle.load(file("/scratch28/diego/b2cc/Bd_MC_TTAU__helcut_" + SW + "_francesca.crap"))
elif SAMPLE == "Bddat": data = cPickle.load(file("/scratch28/diego/b2cc/Bd_dat_helcut_" + SW + "_francesca.crap"))
elif SAMPLE == "Bu": data = cPickle.load(file("/scratch28/diego/b2cc/Bu_MC_TTAU_" + SW + "_francesca.crap"))
elif SAMPLE == "Bs": data = cPickle.load(file("/scratch28/diego/b2cc/Bs_MC" + SW + "_francesca.crap"))
elif SAMPLE == "Budat": data = cPickle.load(file("/scratch28/diego/b2cc/Bu_dat" + SW + "_francesca.crap"))

tmin = 2
tmax = 10.

G = k_TRUE[SAMPLE]

delta = 1e-2

x = np.arange(-2, 2, delta)

def do(t_gpu):
    Ntot = len(t)
    t_gpu = gpuarray.to_gpu(np.float64(t))
    expo_int = pycuda.cumath.exp(G*t_gpu)- np.exp(G*tmin)
    expo_int *= 1./(np.exp(G*tmax)-np.exp(G*tmin))

    def FCN(k):#, epshm):
        
        eff = pycuda.cumath.exp(k*t_gpu) 
 
        ws = 1./eff
        cws =  gpuarray.to_gpu(np.cumsum(ws.get()))
        N0 = np.float64(cws[-1].get())
        cws *= 1./N0
        D = cws - expo_int
        Dmax= np.float64(gpuarray.max(pycuda.cumath.fabs(D)).get())
        KSstat = sqrt(Ntot)*Dmax
        Prob = TMath.KolmogorovProb(KSstat)
        if Prob < 1e-7: chi2 = 25 + KSstat 
        else: chi2 = 2*erfinv(1-Prob)**2

        return chi2

    chi2list = map(FCN,x)
    chi2list = np.float64(chi2list)
    print min(chi2list)
    
    shit = []
    for i in range(len(chi2list)): shit.append([chi2list[i], x[i]])
    shit.sort()
    shit = np.float64(shit)
    xmin = shit[0][1]
    xp = np.extract( shit[:,1] > xmin, shit[:,1])
    xp_ch = np.extract( shit[:,1] > xmin, shit[:,0])
    xn = np.extract( shit[:,1] < xmin, shit[:,1])
    xn_ch = np.extract( shit[:,1] < xmin, shit[:,0])
    chi2min = min(shit[:,0])
    print xmin, chi2min
    return xmin, xn[bisect(xn_ch,chi2min + 1)] - xmin, xp[bisect(xp_ch,chi2min+ 1)] - xmin 

start = timer()
#chi2list = map(contFCN, pairs)
#Z = makeZ(chi2list)
crap = []
for i in range(100):
    t0 = -np.log(rnd.random(10000))
    t0.sort()
    t = np.extract( (t0 >tmin)*(t0<tmax), t0)
    crap.append(do(t))
crap = np.float64(crap)

print "time:" , timer() - start

pull = (crap[:,0]+ G + 1)*2/(np.abs(crap[:,1]) + np.abs(crap[:,2]))



#|    0 |   eps = -0.002944 |  0.001083 |          |          |          |          |          |
#----------------------------------------------------------------------------------------------
#cs = plt.contour(X,Y,Z, levels = [min(chi2list) + 2*1.15, min(chi2list)+5.99,min(chi2list)+11.8 ]) ## 4 is 1.5 sigma

#plt.clabel(cs, inline=1, fontsize=10)
#plt.xlabel("eps_mu")
#plt.ylabel("eps_h")

#plt.show()
