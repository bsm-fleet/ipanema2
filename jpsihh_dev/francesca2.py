from tools import initialize
initialize(0)

from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
from iminuit import Minuit
import pycuda.gpuarray as gpuarray
import cPickle
from math import log
Gs = 0.6614 
DGs = 0.08543 
Gd = 1./ 1.519
Gu = 1./1.638
sf = 5367./5280
SAMPLE = "Bs"

k_TRUE = {"Bd":-Gd, "Bs": - Gs , "Bu": -Gu }
data = {}
data["Bd"] = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bd_francesca_helcut.crap")))
data["Bs"] = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bs_francesca.crap")))
data["Bu"] = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bu_francesca.crap")))

class crap:
    def __init__(self,data):
        self.data_time_gpu = gpuarray.to_gpu(data[:,0])
        self.mup_DZ_gpu = gpuarray.to_gpu(data[:,1])
        self.mum_DZ_gpu = gpuarray.to_gpu(data[:,2])
        self.hp_DZ_gpu = gpuarray.to_gpu(data[:,3])
        self.hm_DZ_gpu = gpuarray.to_gpu(data[:,4])

Bu = crap(data["Bu"])
B = crap(data[SAMPLE])

tmin = 5
tmax = 15.

u_mask = gpuarray.to_gpu(np.float64(( data["Bu"][:,0] > tmin ) * ( data["Bu"][:,0] < tmax)))
d_mask = gpuarray.to_gpu(np.float64(( data[SAMPLE][:,0] > tmin ) * ( data[SAMPLE][:,0] < tmax)))

def eff(B, epsmu, epsh):
    return (1. + epsmu*B.mup_DZ_gpu*B.mup_DZ_gpu) * (1. + epsmu*B.mum_DZ_gpu*B.mum_DZ_gpu)* (1. + epsh*B.hp_DZ_gpu*B.hp_DZ_gpu)* (1. + epsh*B.hm_DZ_gpu*B.hm_DZ_gpu)
def FCN(epsm, epsh):
    #shit = a*data_time_gpu*data_time_gpu*data_time_gpu
    u_eff = eff(Bu,epsm, epsh)
    d_eff = eff(B, epsm, epsh)
    #eff = shit/(1 + shit)
    
    u_w = 1./u_eff*u_mask
    u_sumW = np.sum(u_w.get())
    d_w = 1./d_eff*d_mask
    d_sumW = np.sum(d_w.get())

    def expChi2(B,k,w, sumW):
        if k!= 0 : 
            integral_exp = (np.exp(k*tmax)-np.exp(k*tmin))*1./k
        else : 
            integral_exp = (tmax - tmin)
        
        L_int = -log(integral_exp)
        
        LL_gpu = k*B.data_time_gpu*w 
        
        LL = np.float64(gpuarray.sum(LL_gpu).get()) + sumW*L_int
    
        chi2 = -2*LL
        
        return chi2
        
    
    def FCN1(k, ku):

        chi2 = expChi2(B,k,d_w, d_sumW)
        
        chi2 += expChi2(Bu,ku,u_w, u_sumW)
        
        return chi2
    
    
    fit = Minuit(FCN1, limit_k = (-1.,0), limit_ku = (-1.,0))
    fit.migrad()
    return ((fit.values["k"] - k_TRUE[SAMPLE])/fit.errors["k"])**2 +  ((fit.values["ku"] - k_TRUE["Bu"])/fit.errors["ku"])**2


start = timer()
fit = Minuit(FCN, epsm = -1e-03, epsh = -1e-03, limit_epsm = (-1e-02,0),limit_epsh = (-1e-02,4e-03))# , limit_a = (0,1))
fit.migrad()

print "GPU time:" , timer() - start
