import pycuda.cumath
from timeit import default_timer as timer
from tools import plt
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from math import pi
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import Parameter, Free, Cat
import numpy as np
from PhisModel5 import mod, Badjanak as Model
from phisParams import CSP
PATH = "/home3/veronika.chobanova/GPU/"

import cPickle
BLOCK_SIZE = 30 #(1 - 1024) For complex the max no. smaller

integraB = mod.get_function("binnedTimeIntegralB")
integraBbar = mod.get_function("binnedTimeIntegralBbar")

def getGrid(thiscat, BLOCK_SIZE):
    Nbunch = thiscat.Nevts *1. / BLOCK_SIZE
    if Nbunch > int(Nbunch): 
        Nbunch = int(Nbunch) +1
    else : 
        Nbunch = int(Nbunch)
    return  (Nbunch,1,1)
   
cats, Params = [], []
from math import pi
Params.append(Free("fL",0.5, limits=(0.4,0.6)))
Params.append(Free("fpe",0.25, limits=(0.15,0.35)))
Params.append(Free("phis_0",0.8, limits=(-1.*pi,1.*pi)))#limits=(-pi,pi)))#
Params.append(Parameter("phis_S",0., limits=(-1.,1.)))
Params.append(Free("phis_pa",0., limits=(-1.*pi,1.*pi)))#limits=(-pi,pi)))#
Params.append(Free("phis_pe",0., limits=(-1.*pi,1.*pi)))#limits=(-pi,pi)))#
Params.append(Free("dpa",0., limits=(-pi,pi)))
Params.append(Free("dpe",0., limits=(-pi,pi)))

Params.append(Parameter("G",0.6603, limits=(0.6,0.7)))
Params.append(Parameter("DG",0.0805, limits=(0.04,0.10)))
Params.append(Parameter("Dm",17.7, limits=(16.,19.)))

#Params.append(Free("sigma_t",0.046, limits=(0.03,0.07)))
              
for i in range(1,7):
#for i in range(1,2):
    ibin = str(i)
    Bname = "B_"+ ibin
    Bbarname = "Bbar_"+ ibin
    #if i != 3: continue
    Params.append(Parameter("CSP_" + ibin ,CSP[i]))
    #Params.append(Free("Fs_" + ibin,0.1, limits=(0.,0.8)))
    #Params.append(Free("ds_" + ibin,0., limits=(-10,10.)))
    Params.append(Parameter("Fs_" + ibin,0, limits=(0.,.5)))
    Params.append(Parameter("ds_" + ibin,0., limits=(-10,10.)))

    ## Define category for i-th bin and flavour = B
    #thiscat = Cat(Bname, "data_b_RunI_10M_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_phi_0.5pi_const_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_phi_0.5pi_noconv_"+ibin +".ext", getN = True)
    thiscat = Cat(Bname, PATH + "data_b_phi_0.5pi_delta_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_f0_phi_0.5pi_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_0.5pi_time_res_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_phi_phis0_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_phi_long_only_time_res_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_"+ibin +".crap", getN = True)
    #thiscat = Cat(Bname, "data_b_0.5pi__"+ibin +".ext", getN = True)
    #thiscat = Cat(Bname, "data_b_phi_pa_pe_only_"+ibin +".crap", getN = True)
    
    thiscat.integra = integraB
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
    
    ## Define category for i-th bin and flavour = Bbar
    #thiscat = Cat(Bbarname, "data_bbar_RunI_10M_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_phi_0.5pi_const_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_phi_0.5pi_noconv_"+ibin +".ext", getN = True)
    thiscat = Cat(Bbarname, PATH + "data_bbar_phi_0.5pi_delta_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_f0_phi_0.5pi_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_0.5pi_time_res_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_phi_phis0_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_phi_long_only_time_res_"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_"+ibin +".crap", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_0.5pi__"+ibin +".ext", getN = True)
    #thiscat = Cat(Bbarname, "data_bbar_phi_pa_pe_only_"+ibin +".crap", getN = True)
    
    thiscat.integra = integraBbar
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
     
start = timer()
manager = Model(Params, cats)
manager.createFit()

manager.fit.migrad()
manager.fit.hesse()
#manager.fit.minos()
print manager.fit.get_fmin()['fval']
print timer() - start
manager.plotcat(cats[6])
plt.show()
EXIT

manager.createMultinest("mnest_party", reset = False)
#manager.createMultinest("mnest_party")
values_dic = manager.mnest_vals()
values_list = len(manager.params)*[0.]
for i in xrange(len(manager.params)): 
    values_list[i] = values_dic[manager.params[i]]
manager(*values_list)
    
