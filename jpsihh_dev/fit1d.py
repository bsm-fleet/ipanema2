from tools import initialize
initialize(0)
from bisect import *
from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
from iminuit import Minuit
import pycuda.gpuarray as gpuarray
import cPickle
from math import sqrt, isnan
from ROOT import TMath
from scipy import random as rnd
from scipy.special import erfinv
#from multiprocessing import Pool
from matplotlib import pyplot as plt
from ModelBricks import *
BIN = 4
sBIN = str(BIN)
bins = np.float64([0.3, 0.337264386972, 0.37549669625, 0.414748554299, 0.455075831456, 0.496539120209, 0.53920428282, 0.583143080703, 0.628433900662, 0.675162596446,  0.72342346832, 0.773320408723, 0.824968249017, 0.878494351211, 0.934040500166, 0.991765167029, 1.05184623487, 1.11448430465, 1.17990673635, 1.24837263066, 1.32017902684, 1.39566869131, 1.47524001355, 1.55935973292, 1.64857952745, 1.74355796227, 1.84509002002, 1.95414759126, 2.07193619997, 2.19997646239, 2.34022446712, 2.49525577287, 2.6685581881, 2.86502098649, 3.09180358141, 3.36000284338, 3.68820308685, 4.11122609838, 4.70718114264, 5, 15])
#bins = np.float64([0.3,15])
dataDic = {}


## Thu Aug 30: 5 ps, inconsistent sw's
#epsMCDic = [-13.4, -8.8, -4.2, -4.9]
#epsDatDic = [-3.2, -7.8, -3.8, -6.3]

## Fri Aug 31: 4 ps, consistent sw's
#epsMCDic = [-7.4, -8.2, -3.0, -5.3]
#epsDatDic = [+4.1, -2.5, -3.3, -6.0]
#epsMCDic = 4*[-5.3]
#epsDatDic =4* [-6.0]
epsMCDic = 4*[-4]
epsDatDic =4* [-4]
## Fri Aug 31: 5 ps, consistent sw's
#epsMCDic = [+2.4, -7.6, -3.8, -5.1]
#epsDatDic = [-5.8, -8.2, -4.1, -6.4]

def inte(tmin, tmax, G): 
    k = -G
    if k!= 0 : 
        integral_exp = (np.exp(k*tmax)-np.exp(k*tmin))*1./k
    else : 
        integral_exp = (tmax - tmin)
    return integral_exp
    
        
    


class SimulExpo(ParamBox):
    def __init__(self, pars, cats):
        ParamBox.__init__(self, pars, cats)    
        
    def __call__(self,*args):
            
        chi2 = np.float64(0.)
        N = self.dc
        effs = [len(bins)*[0.],len(bins)*[0.]] 
        for i in range(len(bins)-1):
            effs[0][i] = np.float64(args[N["e0" + str(i)]])
            effs[1][i] = np.float64(args[N["e1" + str(i)]])
  
        GMC = args[N["GMC"]]
        Gdat = args[N["Gdat"]]

        integralMC, integraldat = 0, 0
        LL = 0
        sWtotMC, sWtotdat = 0, 0
        for cat in self.cats:
            i = cat.ibin
            j = cat.trig
            tmin = bins[i]
            tmax = bins[i+1]
            if cat.isMC:
                G = GMC
                integralMC += effs[j][i] * inte(tmin, tmax, G)
                sWtotMC += cat.sWnorm

            else:
                G = Gdat
                integraldat += effs[j][i] * inte(tmin, tmax, G)
                sWtotdat += cat.sWnorm

            
            LL += -G*cat.stuff + np.log(effs[j][i])*cat.sWnorm

        LL += -np.log(integralMC)*sWtotMC - np.log(integraldat)*sWtotdat
        chi2 = -2*LL
        if isnan(chi2): chi2 = 1.e12
        return chi2


def dotest():
    testdat = np.float64(-.678*np.log(rnd.random(1000000)))
    mask = (testdat > bins[0]) * (testdat < bins[1])
    testdat = np.extract(mask, testdat)
    testcat = Cat("test", list(testdat), np.float64(len(testdat)*[0.]))
    testcat.weights =np.float64( len(testdat)*[1.])
    testcat.stuff = np.sum(testcat.data.get() * testcat.weights)
    testcat.ibin = 0
    testcat.sWnorm = np.sum(testcat.weights)
    eps1 = Parameter("e0", 1)
    G = Free("G", -.6)
    manager = Expo([eps1, G], [testcat])
    manager.createFit()
    manager.fit.migrad()

mvar = "B_ConstJpsi_M_1"
dataDic["BdMC0"] = cPickle.load(file("/scratch28/diego/b2cc/Bd_MC_bin"+sBIN+ "_trig0" + mvar +"_helcut_cor_sWeights_Bd_0.3ps.crap"))
dataDic["BdMC1"] = cPickle.load(file("/scratch28/diego/b2cc/Bd_MC_bin"+sBIN+ "_trig1" + mvar +"_helcut_cor_sWeights_Bd_0.3ps.crap"))

dataDic["BdDat0"] = cPickle.load(file("/scratch28/diego/b2cc/Bd_dat_bin"+sBIN+"_trig0" + mvar +"_helcut_cor_sWeights_Bd_0.3ps.crap"))
dataDic["BdDat1"] = cPickle.load(file("/scratch28/diego/b2cc/Bd_dat_bin"+sBIN+"_trig1" + mvar +"_helcut_cor_sWeights_Bd_0.3ps.crap"))

dataMC = [np.float64(dataDic["BdMC0"]), np.float64(dataDic["BdMC1"])] 
data = [np.float64(dataDic["BdDat0"]), np.float64(dataDic["BdDat1"])]

params, cats = [], []
GMC = Parameter("GMC", 1./1.519)
Gdat = Free("Gdat", 1./1.519)

params += [GMC,Gdat]

def initCat(dataG, i, trig, isMC = False):
    data = dataG[trig]
    mask = (data[:,0] > bins[i]) * (data[:,0] < bins[i+1])
    if isMC: epsD = epsMCDic
    else: epsD = epsDatDic
    eps = epsD[BIN-1]*1e-03
    thiscat = Cat("bin" + str(bin), list(np.extract(mask, data[:,0])), np.float64(sum(mask)*[0.]))
    eff = (1 + eps*data[:,1]**2)* (1 + eps*data[:,2]**2)*(1 + eps*data[:,3]**2)*(1 + eps*data[:,4]**2)
    thiscat.weights = np.extract(mask, data[:, 5])*1./np.extract(mask,eff)
    thiscat.stuff = np.sum(thiscat.data.get() * thiscat.weights)
    thiscat.trig = trig
    thiscat.ibin = i
    thiscat.isMC = isMC
    thiscat.sWnorm = np.sum(thiscat.weights)
    return thiscat
    
for i in xrange(len(bins)-1):
    cats.append( initCat(dataMC, i, 0, True))
    #cats.append( initCat(dataMC, i, 1, True))

    cats.append( initCat(data, i, 0,  False) )
    #cats.append( initCat(data, i, 1,  False) )

    if i == 0: params.append(Parameter("e0"+str(i),1))

    else: params.append(Free("e0"+str(i),.5))
    params.append(Free("e1"+str(i),.5))

manager = SimulExpo(params,cats)
manager.createFit()
manager.fit.migrad()


## eps = -4e03
#Gdat =  0.6557  |  0.003436 |
#Gdat = 0.6532  |  0.003081 |         
#Gdat =  0.6646  |  0.002914 |
#Gdat =  0.6602  |  0.002673 

## PT dep. eps Aug 30
#|    1 |  Gdat =  0.6575  |  0.003433 |          |          |          |          |          |
#|    1 |  Gdat =  0.6533  |  0.003067 |          |          |          |          |          |
#|    1 |  Gdat =  0.665   |  0.002915 |          |          |          |          |          |
#|    1 |  Gdat =  0.6581  |  0.002657 |          |          |          |          |          |

## PT dep Aug 31, 4ps
#|    1 |  Gdat =  0.658   |  0.003443 |          |          |          |          |          |
#|    1 |  Gdat =  0.6562  |  0.003083 |          |          |          |          |          |
#|    1 |  Gdat =  0.6645  |  0.002828 |          |          |          |          |          |
#|    1 |  Gdat =  0.6591  |  0.002543 |          |          |          |          |          |
## Aug 31, 5 ps
#|    1 |  Gdat =  0.6541  |  0.003426 |          |          |          |          |          |
#|    1 |  Gdat =  0.6524  |  0.003067 |          |          |          |          |          |
#|    1 |  Gdat =  0.6644  |  0.002813 |          |          |          |          |          |
#|    1 |  Gdat =  0.6581  |  0.002594 |          |          |          |          |          |

## Aug 31, 4ps, PT indep (all like BIN4)
#|    1 |  Gdat =  0.6555  |  0.003433 |          |          |          |          |          |
#|    1 |  Gdat =  0.6526  |  0.003074 |          |          |          |          |          |
#|    1 |  Gdat =  0.6638  |  0.002904 |          |          |          |          |          |
#|    1 |  Gdat =  0.6591  |  0.002543 |          |          |          |          |          |


import sys
sys.path.append("/scratch19/URANIA/URANIA_HEAD/Math/SomeUtils/python/SomeUtils/")
from alyabar import *
def getPval(x, sx):
    a, sa = ponderate(x,sx)
    chi2 = np.sum( (x - a)**2/sx**2)
    return TMath.Prob(chi2,len(x)-1)

#split by trig cat, eps = -4e-03
#|    1 |  Gdat =  0.6535  |  0.002885 |          |          |          |          |          |
#|    1 |  Gdat =  0.6528  |  0.003097 |          |          |          |          |          |
#|    1 |  Gdat =  0.6646  |  0.002482 |          |          |          |          |          |
#|    1 |  Gdat =  0.66    |  0.002452 |          |          |          |          |          |
# p-value 0.0051
# w/a - 0.6585 \pm 0.0013

#split by trig cat, magic vw's

#|    1 |  Gdat =  0.6558  |  0.00335 |          |          |          |          |          |
#|    1 |  Gdat =  0.6559  |  0.002493 |          |          |          |          |          |
#|    1 |  Gdat =  0.6644  |  0.002831 |          |          |          |          |          |
#|    1 |  Gdat =  0.6589  |  0.002432 |          |          |          |          |          |
# p-val = 0.11

# Jenifer M, split by trig cat, eps= -4e-3
#|    1 |  Gdat =  0.6532  |  0.003273 |          |          |          |          |          |
#|    1 |  Gdat =  0.652   |  0.003029 |          |          |          |          |          |
#|    1 |  Gdat =  0.6636  |  0.002876 |          |          |          |          |          |
#|    1 |  Gdat =  0.6602  |  0.00241 |          |          |          |          |          |
#>>> getPval(x, sx)
#0.013495288368773348
#>>> ponderate(x,sx)
#(0.65790670292148135, 0.001420743292389644)
#>>> 

# Jenifer M, split by trig cat, magic vw's
#|    1 |  Gdat =  0.6556  |  0.003296 |          |          |          |          |          |
#|    1 |  Gdat =  0.655   |  0.002628 |          |          |          |          |          |
#|    1 |  Gdat =  0.6635  |  0.002544 |          |          |          |          |          |
#|    1 |  Gdat =  0.6591  |  0.002534 |          |          |          |          |          |
#>>> getPval(x,sx)
#0.09160725627844762
#>>> ponderate(x,sx)
#(0.65866868058262407, 0.0013519780812697025)

 # Jenifer M, split by trig cat, eps= -4e-3, unbiased only
# |    1 |  Gdat =  0.6539  |  0.003821 |          |          |          |          |          |
# |    1 |  Gdat =  0.6534  |  0.003485 |          |          |          |          |          |
# |    1 |  Gdat =  0.6626  |  0.003282 |          |          |          |          |          |
# |    1 |  Gdat =  0.6616  |  0.002845 |          |          |          |          |          |
#>>> getPval(x,sx)
#0.0963362882813651
