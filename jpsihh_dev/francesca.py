from tools import initialize
initialize(0)

from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
from iminuit import Minuit
import pycuda.gpuarray as gpuarray
import cPickle
from math import log
Gs = 0.6614 
DGs = 0.08543 
Gd = 1./ 1.519
Gddat = 1./1.520
Gudat = 1./1.638
Gu = 1./1.638
sf = 5367./5280
SAMPLE = "Bu"

k_TRUE = {"Bd":-Gd, "Bs": - Gs , "Bu": -Gu , "Bddat": -Gddat, "Budat": -Gudat}
if SAMPLE == "Bd": data = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bd_francesca_helcut.crap")))
elif SAMPLE == "Bs": data = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bs_francesca.crap")))
elif SAMPLE == "Bu": data = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bu_francesca.crap")))
elif SAMPLE == "Bddat": data = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bd_dat_helcut_francesca.crap")))
elif SAMPLE == "Budat": data = np.float64(cPickle.load(file("/scratch28/diego/b2cc/Bu_dat_francesca.crap")))
BREAK
tmin = 5
tmax = 15.
mask = (( data[:,0] > tmin ) * ( data[:,0] < tmax))
data_time_gpu = gpuarray.to_gpu(np.extract(mask,data[:,0]))
mup_DZ_gpu = gpuarray.to_gpu(np.extract(mask, data[:,1]))
mum_DZ_gpu = gpuarray.to_gpu(np.extract(mask,data[:,2]))
hp_DZ_gpu = gpuarray.to_gpu(np.extract(mask,data[:,3]))
hm_DZ_gpu = gpuarray.to_gpu(np.extract(mask, data[:,4]))

sw_gpu = gpuarray.to_gpu(np.extract(mask, data[:,5]))


BREAK
#mask = gpuarray.to_gpu(np.float64(( data[:,0] > tmin ) * ( data[:,0] < tmax)))#*sw_gpu

def FCN(eps):
    #shit = a*data_time_gpu*data_time_gpu*data_time_gpu
    eff = (1. + eps*mup_DZ_gpu*mup_DZ_gpu) * (1. + eps*mum_DZ_gpu*mum_DZ_gpu)#* (1. + eps*hp_DZ_gpu*hp_DZ_gpu)* (1. + eps*hm_DZ_gpu*hm_DZ_gpu)
    #eff = shit/(1 + shit)
    
    w = 1./eff*sw_gpu
    sumW = np.sum(w.get())
    def FCN1(k):
        
        if k!= 0 : 
            integral_exp = (np.exp(k*tmax)-np.exp(k*tmin))*1./k
        else : 
            integral_exp = (tmax - tmin)
        
        L_int = -log(integral_exp)
        
        LL_gpu = k*data_time_gpu*w 
        
        #extendLL =  0.# Ndat*math.log(Nexp) -(Nexp)
        LL = np.float64(gpuarray.sum(LL_gpu).get()) + sumW*L_int
    
        chi2 = -2*LL
        return chi2
    
    fit = Minuit(FCN1, limit_k = (-1.,0))
    fit.migrad()
    return( (fit.values["k"] - k_TRUE[SAMPLE])/fit.errors["k"])**2


start = timer()
fit = Minuit(FCN, eps = -1e-03, limit_eps = (-0.01,0))
fit.migrad()

print "GPU time:" , timer() - start
#|    0 |   eps = -0.002944 |  0.001083 |          |          |          |          |          |
#----------------------------------------------------------------------------------------------
