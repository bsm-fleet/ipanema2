#include <stdio.h>
#include <math.h>
// #include <thrust/complex.h>
#include <pycuda-complex.hpp>

#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29
#define eta_bar_OS 0.3791
#define eta_bar_SSK 0.445
#define p0_OS 0.3853
#define p0_SSK 0.450
#define dp0_hf_OS 0.0070
#define dp0_hf_SSK -0.0079
#define p1_OS 0.982
#define p1_SSK 0.976
#define dp1_hf_OS 0.033
#define dp1_hf_SSK 0.004
#define sigma_t_a_RunII -6.//-5.58//
#define sigma_t_b_RunII 1.539//2.28//
#define sigma_t_c_RunII -0.004133//0.//
#define f_sigma_t 0.244
#define r_offset_pr 1.4207
#define r_offset_sc 0.3778
#define r_slope_pr -2.85
#define r_slope_sc -1.55
#define sigma_t_bar 0.0350 
// #define t_0 0.
// #define t_1 14.
#define sigma_t_threshold 30.
#define time_acc_bins 40

__device__ double low_time_acc_bins_ll[time_acc_bins] = {0.3, 0.337264386972, 0.37549669625, 0.414748554299, 0.455075831456, 0.496539120209, 0.53920428282, 0.583143080703, 0.628433900662, 0.675162596446, 0.72342346832, 0.773320408723, 0.824968249017, 0.878494351211, 0.934040500166, 0.991765167029, 1.05184623487, 1.11448430465, 1.17990673635, 1.24837263066, 1.32017902684, 1.39566869131, 1.47524001355, 1.55935973292, 1.64857952745, 1.74355796227, 1.84509002002, 1.95414759126, 2.07193619997, 2.19997646239, 2.34022446712, 2.49525577287, 2.6685581881, 2.86502098649, 3.09180358141, 3.36000284338, 3.68820308685, 4.11122609838, 4.70718114264, 5.724828229};

__device__ double low_time_acc_bins_ul[time_acc_bins] = {0.337264386972, 0.37549669625, 0.414748554299, 0.455075831456, 0.496539120209, 0.53920428282, 0.583143080703, 0.628433900662,  0.675162596446, 0.72342346832, 0.773320408723, 0.824968249017, 0.878494351211, 0.934040500166, 0.991765167029, 1.05184623487, 1.11448430465, 1.17990673635, 1.24837263066, 1.32017902684, 1.39566869131, 1.47524001355, 1.55935973292, 1.64857952745, 1.74355796227, 1.84509002002, 1.95414759126, 2.07193619997, 2.19997646239, 2.34022446712,  2.49525577287, 2.6685581881, 2.86502098649, 3.09180358141, 3.36000284338, 3.68820308685, 4.11122609838, 4.70718114264, 5.724828229,  14.0};

// __device__ double low_time_acc_ub[time_acc_bins] = {0.937478065491, 0.961530566216, 0.971460461617, 0.992898106575, 0.976539313793, 0.975836455822, 0.981001853943, 0.977679431438, 0.993730068207, 0.977258861065, 0.981534957886, 0.980868220329, 0.990724742413, 0.984189987183, 0.993264198303, 0.986189723015, 0.978623628616, 0.984804570675, 0.975276827812, 0.969525754452, 0.957143306732, 0.983526885509, 0.978255867958, 0.9784963727, 0.974327921867, 0.986567616463, 0.987117052078, 0.98275488615, 0.966240048409, 0.98202341795, 0.977456986904, 0.978988409042, 0.977249801159, 0.983859062195, 0.963759243488, 0.984778344631, 0.980318963528, 0.977489650249, 0.968267560005, 0.961861491203};
// 
// __device__ double low_time_acc_eb[time_acc_bins] = {0.200305134058, 0.230256080627, 0.240687444806, 0.288784950972, 0.263598948717, 0.367850810289, 0.36222577095, 0.440307497978, 0.406334519386, 0.448289364576, 0.421891778708, 0.551644980907, 0.478911012411, 0.469617366791, 0.61816906929, 0.473325043917, 0.496003776789, 0.516372203827, 0.554806232452, 0.516676664352, 0.533689916134, 0.4792278409, 0.575142920017, 0.613203704357, 0.542359411716, 0.553472340107, 0.519383609295, 0.516333580017, 0.573337376118, 0.59356456995, 0.629456460476, 0.576147794724, 0.619623720646, 0.663457512856, 0.675717294216, 0.603602647781, 0.606004536152, 0.582402348518, 0.685720086098, 0.686093628407};

__device__ double low_time_acc_ub[time_acc_bins] = {1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.};

__device__ double low_time_acc_eb[time_acc_bins] = {1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.};

__device__ double get_time_acc(double time, int trig_cat)
{    
    int i;
    for(i = 1; i < time_acc_bins; i++)
    {
        if(time < low_time_acc_bins_ll[i])
            break;
    }
    
    double acc;    
    if(trig_cat == 0)
        acc = low_time_acc_ub[i];
    else
        acc = low_time_acc_eb[i];
    
    return acc;
}

__device__ double omega(double eta, int isOS)
{
    double om;
    
    if(isOS == 1)
        om = (p0_OS + dp0_hf_OS) + (p1_OS + dp1_hf_OS)*(eta - eta_bar_OS);
    else
        om = (p0_SSK + dp0_hf_SSK) + (p1_SSK + dp1_hf_SSK)*(eta - eta_bar_SSK);
    
    return om;    
}

__device__ double omega_bar(double eta, int isOS)
{
    double om_bar;
    
    if(isOS == 1)
        om_bar = (p0_OS - dp0_hf_OS) + (p1_OS - dp1_hf_OS)*(eta - eta_bar_OS);
    else
        om_bar = (p0_SSK - dp0_hf_SSK) + (p1_SSK - dp1_hf_SSK)*(eta - eta_bar_SSK);
    
    return om_bar;  
}

__device__ pycuda::complex<double> z(double t, double sigma_t, double G, double Dm)
{
    pycuda::complex<double> I(0,1);
    
    return (-I*(t-sigma_t*sigma_t*G) - Dm*sigma_t*sigma_t)/(sigma_t*sqrt(2.));
}

__device__ double delta_RunII(double sigma_t)
{
    return sigma_t_a_RunII*sigma_t*sigma_t + sigma_t_b_RunII*sigma_t + sigma_t_c_RunII;
}

__device__ double delta_1(double sigma_t)
{
    return -sqrt(f_sigma_t/(1.-f_sigma_t))*(r_offset_sc+r_slope_sc*(sigma_t-sigma_t_bar))*sigma_t
                                         + (r_offset_pr+r_slope_pr*(sigma_t-sigma_t_bar))*sigma_t;
}

__device__ double delta_2(double sigma_t)
{
    return sqrt((1.-f_sigma_t)/f_sigma_t)*(r_offset_sc+r_slope_sc*(sigma_t-sigma_t_bar))*sigma_t
                                        + (r_offset_pr+r_slope_pr*(sigma_t-sigma_t_bar))*sigma_t;
}

__device__ pycuda::complex<double> faddeeva(pycuda::complex<double> z, double t) 
{
   double in_real = pycuda::real(z);
   double in_imag = pycuda::imag(z);
   int n, nc, nu;
   double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
   double Rx [33];
   double Ry [33];

   x = fabs(in_real);
   y = fabs(in_imag);

   if (y < yLim && x < xLim) {
      q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
      h  = 1.0 / (3.2 * q);
      nc = 7 + int(23.0 * q);
//       xl = pow(h, double(1 - nc));
      double h_inv = 1./h;
      xl = h_inv;
      for(int i = 1; i < nc-1; i++)
          xl *= h_inv;
      
      xh = y + 0.5 / h;
      yh = x;
      nu = 10 + int(21.0 * q);
      Rx[nu] = 0.;
      Ry[nu] = 0.;
      for (n = nu; n > 0; n--){
         Tx = xh + n * Rx[n];
         Ty = yh - n * Ry[n];
         Tn = Tx*Tx + Ty*Ty;
         Rx[n-1] = 0.5 * Tx / Tn;
         Ry[n-1] = 0.5 * Ty / Tn;
         }
      Sx = 0.;
      Sy = 0.;
      for (n = nc; n>0; n--){
         Saux = Sx + xl;
         Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
         Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
         xl = h * xl;
      };
      Wx = errf_const * Sx;
      Wy = errf_const * Sy;
   }
   else {
      xh = y;
      yh = x;
      Rx[0] = 0.;
      Ry[0] = 0.;
      for (n = 9; n>0; n--){
         Tx = xh + n * Rx[0];
         Ty = yh - n * Ry[0];
         Tn = Tx * Tx + Ty * Ty;
         Rx[0] = 0.5 * Tx / Tn;
         Ry[0] = 0.5 * Ty / Tn;
      };
      Wx = errf_const * Rx[0];
      Wy = errf_const * Ry[0];
   }

   if (y == 0.) {
      Wx = exp(-x * x);
   }
   if (in_imag < 0.) {
//       Wx =   2.0 * exp(y * y - x * x) * cos(2.0 * x * y) - Wx;
//       Wy = - 2.0 * exp(y * y - x * x) * sin(2.0 * x * y) - Wy;     
      double exp_x2_y2 = exp(y * y - x * x);
      Wx =   2.0 * exp_x2_y2 * cos(2.0 * x * y) - Wx;
      Wy = - 2.0 * exp_x2_y2 * sin(2.0 * x * y) - Wy;
      if (in_real > 0.) {
         Wy = -Wy;
      }
   }
   else if (in_real < 0.) {
      Wy = -Wy;
   }

   return pycuda::complex<double>(Wx,Wy);
}

__device__ double conv_exp_single(double t, double G, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    if(t>sigma_t_threshold*sigma_t){
        double f = 2.*(sqrt(0.5*M_PI)); 
                   
        return f*exp(-G*t+0.5*G*G*sigma_t_sq);
    }
    else
        return sqrt(0.5*M_PI)*exp(-G*t+0.5*G*sigma_t_sq)*(1.+erf((t-G*sigma_t_sq)/(sigma_t*sqrt(2.)))); 
}

__device__ double conv_exp_cos_single(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){        
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*cos(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::real(faddeeva(z,t));
    }
}

__device__ double conv_exp_sin_single(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_sq = sigma_t*sigma_t;
    
    pycuda::complex<double> I(0,1);
    
    if(t>sigma_t_threshold*sigma_t){
        double f  = 2.*(sqrt(0.5*M_PI));
        
        return f*exp(-G*t+0.5*G*G*sigma_t_sq-0.5*Dm*Dm*sigma_t_sq)*sin(Dm*(t-G*sigma_t_sq));
    }else{
        pycuda::complex<double> z = (-I*(t-sigma_t_sq*G) - Dm*sigma_t_sq)/(sigma_t*sqrt(2.));
        
        return -sqrt(0.5*M_PI)*exp(-0.5*t*t/sigma_t_sq)*pycuda::imag(faddeeva(z,t));
    }
}

__device__ double conv_cosh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)+conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_sinh_single(double t, double G, double DG, double sigma_t) 
{
    double G_m_hDG = G - 0.5*DG;
    double G_p_hDG = G + 0.5*DG;
    
    return 0.5*(conv_exp_single(t,G_m_hDG,sigma_t)-conv_exp_single(t,G_p_hDG,sigma_t));
}

__device__ double conv_exp(double t, double G, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_exp_single(t, G, sigma_t_fcn1) 
    + (1.-f_sigma_t)*conv_exp_single(t, G, sigma_t_fcn2); 
}

__device__ double conv_exp_cos(double t, double G, double Dm, double sigma_t) 
{   
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_exp_cos_single(t, G, Dm, sigma_t_fcn1)
    + (1.-f_sigma_t)*conv_exp_cos_single(t, G, Dm, sigma_t_fcn2);
}

__device__ double conv_exp_sin(double t, double G, double Dm, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_exp_sin_single(t, G, Dm, sigma_t_fcn1)
    + (1.-f_sigma_t)*conv_exp_sin_single(t, G, Dm, sigma_t_fcn2);
}

__device__ double conv_cosh(double t, double G, double DG, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t); 
    
    return f_sigma_t*conv_cosh_single(t, G, DG, sigma_t_fcn1) 
    + (1.-f_sigma_t)*conv_cosh_single(t, G, DG, sigma_t_fcn2);
}

__device__ double conv_sinh(double t, double G, double DG, double sigma_t) 
{
    double sigma_t_fcn1 = delta_1(sigma_t);
    double sigma_t_fcn2 = delta_2(sigma_t);
    
    return f_sigma_t*conv_sinh_single(t, G, DG, sigma_t_fcn1) 
    + (1.-f_sigma_t)*conv_sinh_single(t, G, DG, sigma_t_fcn2);
}

__device__ double Nk(double A_0_abs, 
                     double A_S_abs,
                     double A_pa_abs, 
                     double A_pe_abs,
                     double CSP,
                     int k)
{
    double nk;
    switch(k) {
        case 1:  nk = A_0_abs*A_0_abs;
                 break;
        case 2:  nk = A_pa_abs*A_pa_abs;
                 break;
        case 3:  nk = A_pe_abs*A_pe_abs;
                 break;
        case 4:  nk = A_pe_abs*A_pa_abs;
                 break;
        case 5:  nk = A_0_abs*A_pa_abs;
                 break;
        case 6:  nk = A_0_abs*A_pe_abs;
                 break;
        case 7:  nk = A_S_abs*A_S_abs;
                 break;
        case 8:  nk = CSP*A_S_abs*A_pa_abs;
                 break;
        case 9:  nk = CSP*A_S_abs*A_pe_abs;
                 break;
        case 10: nk = CSP*A_S_abs*A_0_abs;
                 break;
        default: printf("Wrong k index in nk, please check code %d\\n", k);
                 return 0.;
    }
    return nk;
}

__device__ double fk(double helcosthetaK, 
                     double helcosthetaL, 
                     double helphi,
                     int k)
{
    double helsinthetaK = sqrt(1. - helcosthetaK*helcosthetaK);
    double helsinthetaL = sqrt(1. - helcosthetaL*helcosthetaL);
//     helphi -= M_PI;
    double helsinphi = sin(-helphi);
    double helcosphi = cos(-helphi);
    
    double fk;
    switch(k) {
        case 1:  fk = helcosthetaK*helcosthetaK*helsinthetaL*helsinthetaL;
                 break;
        case 2:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helcosphi*helcosphi*helsinthetaL*helsinthetaL);
                 break;
        case 3:  fk = 0.5*helsinthetaK*helsinthetaK*(1.-helsinphi*helsinphi*helsinthetaL*helsinthetaL);
                 break;
        case 4:  fk = helsinthetaK*helsinthetaK*helsinthetaL*helsinthetaL*helsinphi*helcosphi;
                 break;
        case 5:  fk = sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helcosphi;
                 break;
        case 6:  fk = -sqrt(2.)*helsinthetaK*helcosthetaK*helsinthetaL*helcosthetaL*helsinphi;
                 break;
        case 7:  fk = helsinthetaL*helsinthetaL/3.;
                 break;
        case 8:  fk = 2.*helsinthetaK*helsinthetaL*helcosthetaL*helcosphi/sqrt(6.);
                 break;
        case 9:  fk = -2.*helsinthetaK*helsinthetaL*helcosthetaL*helsinphi/sqrt(6.);
                 break;
        case 10: fk = 2.*helcosthetaK*helsinthetaL*helsinthetaL/sqrt(3.);
                 break;
        default: printf("Wrong k index in fk, please check code %d\\n", k);
                 return 0.;
    }
    return fk;
}

__device__ double ak(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double ak;
    switch(k) {
        case 1:  ak = 0.5*(1.+lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ak = 0.5*(1.+lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ak = 0.5*(1.+lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ak = 0.5*(sin(delta_pe-delta_pa) - lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ak = 0.5*(cos(delta_0-delta_pa) + lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ak = -0.5*(sin(delta_0-delta_pe) - lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ak = 0.5*(1.+lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ak = 0.5*(cos(delta_S-delta_pa) - lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ak = -0.5*(sin(delta_S-delta_pe) + lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ak = 0.5*(cos(delta_S-delta_0) - lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ak, please check code %d\\n", k);
                 return 0.;
    }
    return ak;
}

__device__ double bk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    double bk;
    switch(k) {
        case 1:  bk = -lambda_0_abs*cos(phis_0);
                 break;
        case 2:  bk = -lambda_pa_abs*cos(phis_pa);
                 break;
        case 3:  bk = lambda_pe_abs*cos(phis_pe);
                 break;
        case 4:  bk = 0.5*(lambda_pe_abs*sin(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*sin(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  bk = -0.5*(lambda_0_abs*cos(delta_0-delta_pa-phis_0) + lambda_pa_abs*cos(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  bk = 0.5*(lambda_0_abs*sin(delta_0-delta_pe-phis_0) + lambda_pe_abs*sin(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  bk = lambda_S_abs*cos(phis_S);
                 break;
        case 8:  bk = 0.5*(lambda_S_abs*cos(delta_S-delta_pa-phis_S) - lambda_pa_abs*cos(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  bk = -0.5*(lambda_S_abs*sin(delta_S-delta_pe-phis_S) - lambda_pe_abs*sin(delta_pe-delta_S-phis_pe));
                 break;
        case 10: bk = 0.5*(lambda_S_abs*cos(delta_S-delta_0-phis_S) - lambda_0_abs*cos(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in bk, please check code %d\\n", k);
                 return 0.;
    }
    return bk;
}
    
__device__ double ck(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double ck;
    switch(k) {
        case 1:  ck = 0.5*(1.-lambda_0_abs*lambda_0_abs);
                 break;
        case 2:  ck = 0.5*(1.-lambda_pa_abs*lambda_pa_abs);
                 break;
        case 3:  ck = 0.5*(1.-lambda_pe_abs*lambda_pe_abs);
                 break;
        case 4:  ck = 0.5*(sin(delta_pe-delta_pa) + lambda_pe_abs*lambda_pa_abs*sin(delta_pe-delta_pa-phis_pe+phis_pa));
                 break;
        case 5:  ck = 0.5*(cos(delta_0-delta_pa) - lambda_0_abs*lambda_pa_abs*cos(delta_0-delta_pa-phis_0+phis_pa));
                 break;
        case 6:  ck = -0.5*(sin(delta_0-delta_pe) + lambda_0_abs*lambda_pe_abs*sin(delta_0-delta_pe-phis_0+phis_pe));
                 break;
        case 7:  ck = 0.5*(1.-lambda_S_abs*lambda_S_abs);
                 break;
        case 8:  ck = 0.5*(cos(delta_S-delta_pa) + lambda_S_abs*lambda_pa_abs*cos(delta_S-delta_pa-phis_S+phis_pa));
                 break;
        case 9:  ck = -0.5*(sin(delta_S-delta_pe) - lambda_S_abs*lambda_pe_abs*sin(delta_S-delta_pe-phis_S+phis_pe));
                 break;
        case 10: ck = 0.5*(cos(delta_S-delta_0) + lambda_S_abs*lambda_0_abs*cos(delta_S-delta_0-phis_S+phis_0));
                 break;
        default: printf("Wrong k index in ck, please check code %d\\n", k);
                 return 0.;
    }
    return ck;
}

__device__ double dk(double phis_0,
                     double phis_S,
                     double phis_pa,
                     double phis_pe,
                     double delta_0,
                     double delta_S,
                     double delta_pa,
                     double delta_pe,
                     double lambda_0_abs,
                     double lambda_S_abs,
                     double lambda_pa_abs,
                     double lambda_pe_abs,
                     int k)
{
    
    double dk;
    switch(k) {
        case 1:  dk = lambda_0_abs*sin(phis_0);
                 break;
        case 2:  dk = lambda_pa_abs*sin(phis_pa);
                 break;
        case 3:  dk = -lambda_pe_abs*sin(phis_pe);
                 break;
        case 4:  dk = -0.5*(lambda_pe_abs*cos(delta_pe-delta_pa-phis_pe) + lambda_pa_abs*cos(delta_pa-delta_pe-phis_pa));
                 break;
        case 5:  dk = -0.5*(lambda_0_abs*sin(delta_0-delta_pa-phis_0) + lambda_pa_abs*sin(delta_pa-delta_0-phis_pa));
                 break;
        case 6:  dk = -0.5*(lambda_0_abs*cos(delta_0-delta_pe-phis_0) + lambda_pe_abs*cos(delta_pe-delta_0-phis_pe));
                 break;
        case 7:  dk = -lambda_S_abs*sin(phis_S);
                 break;
        case 8:  dk = 0.5*(lambda_S_abs*sin(delta_S-delta_pa-phis_S) - lambda_pa_abs*sin(delta_pa-delta_S-phis_pa));
                 break;
        case 9:  dk = -0.5*(-lambda_S_abs*cos(delta_S-delta_pe-phis_S) + lambda_pe_abs*cos(delta_pe-delta_S-phis_pe));
                 break;
        case 10: dk = 0.5*(lambda_S_abs*sin(delta_S-delta_0-phis_S) - lambda_0_abs*sin(delta_0-delta_S-phis_0));
                 break;
        default: printf("Wrong k index in dk, please check code %d\\n", k);
                 return 0.;
    }
    return dk;
}

__device__ double integral4pitimeB( double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t, double trig_cat)
{
    double G_sq = G*G;
    double DG_sq = DG*DG;
    double DM_sq = DM*DM;
    
    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;
    
    double lambda_0_abs_sq = lambda_0_abs*lambda_0_abs;
    double lambda_pa_abs_sq = lambda_pa_abs*lambda_pa_abs;
    double lambda_pe_abs_sq = lambda_pe_abs*lambda_pe_abs;
    double lambda_S_abs_sq = lambda_S_abs*lambda_S_abs;
    
    double sin_phi_0 = sin(phi_0);
    double sin_phi_pa = sin(phi_pa);
    double sin_phi_pe = sin(phi_pe);
    double sin_phi_S = sin(phi_S);
    
    double cos_phi_0 = cos(phi_0);
    double cos_phi_pa = cos(phi_pa);
    double cos_phi_pe = cos(phi_pe);
    double cos_phi_S = cos(phi_S);

    double integral = 0.;
    
    double low_time_acc;
    
    for(int i=0; i<time_acc_bins; i++)
    {
        double t_0 = low_time_acc_bins_ll[i];
        double t_1 = low_time_acc_bins_ul[i];
        
//         double conv_sin_term_t_0  = conv_exp_sin(t_0, G, DM, sigma_t);
//         double conv_cos_term_t_0  = conv_exp_cos(t_0, G, DM, sigma_t);
//         double conv_sinh_term_t_0 = conv_sinh(t_0, G, DG, sigma_t);
//         double conv_cosh_term_t_0 = conv_cosh(t_0, G, DG, sigma_t);
//         
//         double conv_sin_term_t_1  = conv_exp_sin(t_1, G, DM, sigma_t);
//         double conv_cos_term_t_1  = conv_exp_cos(t_1, G, DM, sigma_t);
//         double conv_sinh_term_t_1 = conv_sinh(t_1, G, DG, sigma_t);
//         double conv_cosh_term_t_1 = conv_cosh(t_1, G, DG, sigma_t);
        
        double conv_sin_term_t_0  = conv_exp_sin_single(t_0, G, DM, sigma_t);
        double conv_cos_term_t_0  = conv_exp_cos_single(t_0, G, DM, sigma_t);
        double conv_sinh_term_t_0 = conv_sinh_single(t_0, G, DG, sigma_t);
        double conv_cosh_term_t_0 = conv_cosh_single(t_0, G, DG, sigma_t);
        
        double conv_sin_term_t_1  = conv_exp_sin_single(t_1, G, DM, sigma_t);
        double conv_cos_term_t_1  = conv_exp_cos_single(t_1, G, DM, sigma_t);
        double conv_sinh_term_t_1 = conv_sinh_single(t_1, G, DG, sigma_t);
        double conv_cosh_term_t_1 = conv_cosh_single(t_1, G, DG, sigma_t);
        
        if(trig_cat==0)
            low_time_acc = low_time_acc_ub[i];
        else
            low_time_acc = low_time_acc_eb[i];
        
        integral += low_time_acc*(-2.0*A_0_mod_2*lambda_0_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_0 
                        + 8.0*A_0_mod_2*lambda_0_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_0 
                        + A_0_mod_2*(DG_sq - 4.0*G_sq)*(lambda_0_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_0_mod_2*(DM_sq + G_sq)*(lambda_0_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_S_mod_2*lambda_S_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_S 
                        - 8.0*A_S_mod_2*lambda_S_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_S 
                        + A_S_mod_2*(DG_sq - 4.0*G_sq)*(lambda_S_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_S_mod_2*(DM_sq + G_sq)*(lambda_S_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        - 2.0*A_pa_mod_2*lambda_pa_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pa 
                        + 8.0*A_pa_mod_2*lambda_pa_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pa 
                        + A_pa_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pa_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pa_mod_2*(DM_sq + G_sq)*(lambda_pa_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_pe_mod_2*lambda_pe_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pe 
                        - 8.0*A_pe_mod_2*lambda_pe_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pe 
                        + A_pe_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pe_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pe_mod_2*(DM_sq + G_sq)*(lambda_pe_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)))
            /((DG_sq - 4*G_sq)*(DM_sq + G_sq)); 
    }

return integral;
}

__device__ double integral4pitimeBbar( double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t, double trig_cat )
{ 
    double G_sq = G*G;
    double DG_sq = DG*DG;
    double DM_sq = DM*DM;
    
    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;
    
    double lambda_0_abs_sq = lambda_0_abs*lambda_0_abs;
    double lambda_pa_abs_sq = lambda_pa_abs*lambda_pa_abs;
    double lambda_pe_abs_sq = lambda_pe_abs*lambda_pe_abs;
    double lambda_S_abs_sq = lambda_S_abs*lambda_S_abs;
    
    double sin_phi_0 = sin(phi_0);
    double sin_phi_pa = sin(phi_pa);
    double sin_phi_pe = sin(phi_pe);
    double sin_phi_S = sin(phi_S);
    
    double cos_phi_0 = cos(phi_0);
    double cos_phi_pa = cos(phi_pa);
    double cos_phi_pe = cos(phi_pe);
    double cos_phi_S = cos(phi_S);

    double integral = 0.;
    
    double low_time_acc;
    
    for(int i=0; i<time_acc_bins; i++)
    {
        double t_0 = low_time_acc_bins_ll[i];
        double t_1 = low_time_acc_bins_ul[i];
        
//         double conv_sin_term_t_0  = conv_exp_sin(t_0, G, DM, sigma_t);
//         double conv_cos_term_t_0  = conv_exp_cos(t_0, G, DM, sigma_t);
//         double conv_sinh_term_t_0 = conv_sinh(t_0, G, DG, sigma_t);
//         double conv_cosh_term_t_0 = conv_cosh(t_0, G, DG, sigma_t);
//         
//         double conv_sin_term_t_1  = conv_exp_sin(t_1, G, DM, sigma_t);
//         double conv_cos_term_t_1  = conv_exp_cos(t_1, G, DM, sigma_t);
//         double conv_sinh_term_t_1 = conv_sinh(t_1, G, DG, sigma_t);
//         double conv_cosh_term_t_1 = conv_cosh(t_1, G, DG, sigma_t);
        
        double conv_sin_term_t_0  = conv_exp_sin_single(t_0, G, DM, sigma_t);
        double conv_cos_term_t_0  = conv_exp_cos_single(t_0, G, DM, sigma_t);
        double conv_sinh_term_t_0 = conv_sinh_single(t_0, G, DG, sigma_t);
        double conv_cosh_term_t_0 = conv_cosh_single(t_0, G, DG, sigma_t);
        
        double conv_sin_term_t_1  = conv_exp_sin_single(t_1, G, DM, sigma_t);
        double conv_cos_term_t_1  = conv_exp_cos_single(t_1, G, DM, sigma_t);
        double conv_sinh_term_t_1 = conv_sinh_single(t_1, G, DG, sigma_t);
        double conv_cosh_term_t_1 = conv_cosh_single(t_1, G, DG, sigma_t);
        
        if(trig_cat==0)
            low_time_acc = low_time_acc_ub[i];
        else
            low_time_acc = low_time_acc_eb[i];
        
        integral += low_time_acc*(2.0*A_0_mod_2*lambda_0_abs*(DG_sq - 4.0*G_sq)
                            *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_0 
                            + 8.0*A_0_mod_2*lambda_0_abs*(DM_sq + G_sq)
                            *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_0 
                            - A_0_mod_2*(DG_sq - 4.0*G_sq)*(lambda_0_abs_sq - 1.0)
                            *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                            - 4.0*A_0_mod_2*(DM_sq + G_sq)*(lambda_0_abs_sq + 1.0)
                            *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                            - 2.0*A_S_mod_2*lambda_S_abs*(DG_sq - 4.0*G_sq)
                            *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_S 
                            - 8.0*A_S_mod_2*lambda_S_abs*(DM_sq + G_sq)
                            *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_S 
                            - A_S_mod_2*(DG_sq - 4.0*G_sq)*(lambda_S_abs_sq - 1.0)
                            *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                            - 4.0*A_S_mod_2*(DM_sq + G_sq)*(lambda_S_abs_sq + 1.0)
                            *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                            + 2.0*A_pa_mod_2*lambda_pa_abs*(DG_sq - 4.0*G_sq)
                            *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pa 
                            + 8.0*A_pa_mod_2*lambda_pa_abs*(DM_sq + G_sq)
                            *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pa 
                            - A_pa_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pa_abs_sq - 1.0)
                            *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                            - 4.0*A_pa_mod_2*(DM_sq + G_sq)*(lambda_pa_abs_sq + 1.0)
                            *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                            - 2.0*A_pe_mod_2*lambda_pe_abs*(DG_sq - 4.0*G_sq)
                            *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pe 
                            - 8.0*A_pe_mod_2*lambda_pe_abs*(DM_sq + G_sq)
                            *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pe 
                            - A_pe_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pe_abs_sq - 1.0)
                            *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                            - 4.0*A_pe_mod_2*(DM_sq + G_sq)*(lambda_pe_abs_sq + 1.0)
                            *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)))
                    /((DG_sq - 4*G_sq)*(DM_sq + G_sq));
    }

return integral;
}
 

__device__ void integral4pitime( double integral[2], double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t, double trig_cat )
{ 
    double G_sq = G*G;
    double DG_sq = DG*DG;
    double DM_sq = DM*DM;
    
    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;
    
    double lambda_0_abs_sq = lambda_0_abs*lambda_0_abs;
    double lambda_pa_abs_sq = lambda_pa_abs*lambda_pa_abs;
    double lambda_pe_abs_sq = lambda_pe_abs*lambda_pe_abs;
    double lambda_S_abs_sq = lambda_S_abs*lambda_S_abs;
    
    double sin_phi_0 = sin(phi_0);
    double sin_phi_pa = sin(phi_pa);
    double sin_phi_pe = sin(phi_pe);
    double sin_phi_S = sin(phi_S);
    
    double cos_phi_0 = cos(phi_0);
    double cos_phi_pa = cos(phi_pa);
    double cos_phi_pe = cos(phi_pe);
    double cos_phi_S = cos(phi_S);
    
    double low_time_acc;
    double t_0, t_1;
    double conv_sin_term_t_0, conv_cos_term_t_0, conv_sinh_term_t_0, conv_cosh_term_t_0;
    double conv_sin_term_t_1, conv_cos_term_t_1, conv_sinh_term_t_1, conv_cosh_term_t_1;

    integral[0] = 0.;
    integral[1] = 0.;
    
    for(int i=0; i<time_acc_bins; i++)
    {
        t_0 = low_time_acc_bins_ll[i];
        t_1 = low_time_acc_bins_ul[i];
        
//         conv_sin_term_t_0  = conv_exp_sin(t_0, G, DM, sigma_t);
//         conv_cos_term_t_0  = conv_exp_cos(t_0, G, DM, sigma_t);
//         conv_sinh_term_t_0 = conv_sinh(t_0, G, DG, sigma_t);
//         conv_cosh_term_t_0 = conv_cosh(t_0, G, DG, sigma_t);
//         
//         conv_sin_term_t_1  = conv_exp_sin(t_1, G, DM, sigma_t);
//         conv_cos_term_t_1  = conv_exp_cos(t_1, G, DM, sigma_t);
//         conv_sinh_term_t_1 = conv_sinh(t_1, G, DG, sigma_t);
//         conv_cosh_term_t_1 = conv_cosh(t_1, G, DG, sigma_t);
        
        conv_sin_term_t_0  = conv_exp_sin_single(t_0, G, DM, sigma_t);
        conv_cos_term_t_0  = conv_exp_cos_single(t_0, G, DM, sigma_t);
        conv_sinh_term_t_0 = conv_sinh_single(t_0, G, DG, sigma_t);
        conv_cosh_term_t_0 = conv_cosh_single(t_0, G, DG, sigma_t);
        
        conv_sin_term_t_1  = conv_exp_sin_single(t_1, G, DM, sigma_t);
        conv_cos_term_t_1  = conv_exp_cos_single(t_1, G, DM, sigma_t);
        conv_sinh_term_t_1 = conv_sinh_single(t_1, G, DG, sigma_t);
        conv_cosh_term_t_1 = conv_cosh_single(t_1, G, DG, sigma_t);
        
        if(trig_cat==0)
            low_time_acc = low_time_acc_ub[i];
        else
            low_time_acc = low_time_acc_eb[i];
        
        integral[0] +=  low_time_acc*(-2.0*A_0_mod_2*lambda_0_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_0 
                        + 8.0*A_0_mod_2*lambda_0_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_0 
                        + A_0_mod_2*(DG_sq - 4.0*G_sq)*(lambda_0_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_0_mod_2*(DM_sq + G_sq)*(lambda_0_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_S_mod_2*lambda_S_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_S 
                        - 8.0*A_S_mod_2*lambda_S_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_S 
                        + A_S_mod_2*(DG_sq - 4.0*G_sq)*(lambda_S_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_S_mod_2*(DM_sq + G_sq)*(lambda_S_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        - 2.0*A_pa_mod_2*lambda_pa_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pa 
                        + 8.0*A_pa_mod_2*lambda_pa_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pa 
                        + A_pa_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pa_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pa_mod_2*(DM_sq + G_sq)*(lambda_pa_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                        + 2.0*A_pe_mod_2*lambda_pe_abs*(DG_sq - 4.0*G_sq)
                        *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pe 
                        - 8.0*A_pe_mod_2*lambda_pe_abs*(DM_sq + G_sq)
                        *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pe 
                        + A_pe_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pe_abs_sq - 1.0)
                        *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                        - 4.0*A_pe_mod_2*(DM_sq + G_sq)*(lambda_pe_abs_sq + 1.0)
                        *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)))
                /((DG_sq - 4*G_sq)*(DM_sq + G_sq)); 
        
        integral[1] += low_time_acc*(2.0*A_0_mod_2*lambda_0_abs*(DG_sq - 4.0*G_sq)
                       *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_0 
                       + 8.0*A_0_mod_2*lambda_0_abs*(DM_sq + G_sq)
                       *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_0 
                       - A_0_mod_2*(DG_sq - 4.0*G_sq)*(lambda_0_abs_sq - 1.0)
                       *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                       - 4.0*A_0_mod_2*(DM_sq + G_sq)*(lambda_0_abs_sq + 1.0)
                       *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                       - 2.0*A_S_mod_2*lambda_S_abs*(DG_sq - 4.0*G_sq)
                       *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_S 
                       - 8.0*A_S_mod_2*lambda_S_abs*(DM_sq + G_sq)
                       *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_S 
                       - A_S_mod_2*(DG_sq - 4.0*G_sq)*(lambda_S_abs_sq - 1.0)
                       *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                       - 4.0*A_S_mod_2*(DM_sq + G_sq)*(lambda_S_abs_sq + 1.0)
                       *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                       + 2.0*A_pa_mod_2*lambda_pa_abs*(DG_sq - 4.0*G_sq)
                       *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pa 
                       + 8.0*A_pa_mod_2*lambda_pa_abs*(DM_sq + G_sq)
                       *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pa 
                       - A_pa_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pa_abs_sq - 1.0)
                       *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                       - 4.0*A_pa_mod_2*(DM_sq + G_sq)*(lambda_pa_abs_sq + 1.0)
                       *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)) 
                       - 2.0*A_pe_mod_2*lambda_pe_abs*(DG_sq - 4.0*G_sq)
                       *((-DM*conv_cos_term_t_0 - G*conv_sin_term_t_0) + (DM*conv_cos_term_t_1 + G*conv_sin_term_t_1))*sin_phi_pe 
                       - 8.0*A_pe_mod_2*lambda_pe_abs*(DM_sq + G_sq)
                       *((0.5*DG*conv_cosh_term_t_0 + G*conv_sinh_term_t_0) + (-0.5*DG*conv_cosh_term_t_1 - G*conv_sinh_term_t_1))*cos_phi_pe 
                       - A_pe_mod_2*(DG_sq - 4.0*G_sq)*(lambda_pe_abs_sq - 1.0)
                       *(-DM*conv_sin_term_t_1 + DM*conv_sin_term_t_0 + G*conv_cos_term_t_1 - G*conv_cos_term_t_0) 
                       - 4.0*A_pe_mod_2*(DM_sq + G_sq)*(lambda_pe_abs_sq + 1.0)
                       *((0.5*DG*conv_sinh_term_t_0 + G*conv_cosh_term_t_0) + (-0.5*DG*conv_sinh_term_t_1 - G*conv_cosh_term_t_1)))
                /((DG_sq - 4*G_sq)*(DM_sq + G_sq));
    }
} 
 
__device__ double integral4piB( double t, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t, double trig_cat )
{
//      double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
//      double exp_sin_term  = conv_exp_sin(t, G, DM, sigma_t);
//      double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
     
    double exp_cosh_term = conv_cosh_single(t, G, DG, sigma_t);
    double exp_sinh_term = conv_sinh_single(t, G, DG, sigma_t);
    double exp_sin_term  = conv_exp_sin_single(t, G, DM, sigma_t);

    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;

    double num =  2.0*exp_cosh_term* (A_0_mod_2 + A_S_mod_2 + A_pa_mod_2 + A_pe_mod_2)
                + 2.0*exp_sin_term*  (A_0_mod_2*sin(phi_0) - A_S_mod_2*sin(phi_S) + A_pa_mod_2*sin(phi_pa) - A_pe_mod_2*sin(phi_pe))
                + 2.0*exp_sinh_term*(-A_0_mod_2*cos(phi_0) + A_S_mod_2*cos(phi_S) - A_pa_mod_2*cos(phi_pa) + A_pe_mod_2*cos(phi_pe));

    double time_acc = get_time_acc(t, trig_cat);
    
    return time_acc*num/integral4pitimeB( A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0, phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigma_t, trig_cat);
}

__device__ double integral4piBbar( double t, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, double sigma_t, int trig_cat )
{
//      double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
//      double exp_sin_term  = conv_exp_sin(t, G, DM, sigma_t);
//      double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
     
    double exp_cosh_term = conv_cosh_single(t, G, DG, sigma_t);
    double exp_sinh_term = conv_sinh_single(t, G, DG, sigma_t);
    double exp_sin_term  = conv_exp_sin_single(t, G, DM, sigma_t);

    double A_0_mod_2 = A_0_mod*A_0_mod;
    double A_pa_mod_2 = A_pa_mod*A_pa_mod;
    double A_pe_mod_2 = A_pe_mod*A_pe_mod;
    double A_S_mod_2 = A_S_mod*A_S_mod;

    double num =  2.0*exp_cosh_term* (A_0_mod_2 + A_S_mod_2 + A_pa_mod_2 + A_pe_mod_2)
                + 2.0*exp_sin_term* (-A_0_mod_2*sin(phi_0) + A_S_mod_2*sin(phi_S) - A_pa_mod_2*sin(phi_pa) + A_pe_mod_2*sin(phi_pe))
                + 2.0*exp_sinh_term*(-A_0_mod_2*cos(phi_0) + A_S_mod_2*cos(phi_S) - A_pa_mod_2*cos(phi_pa) + A_pe_mod_2*cos(phi_pe));

    double time_acc = get_time_acc(t, trig_cat);
    
    return time_acc*num/integral4pitimeBbar( A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0, phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigma_t, trig_cat);
}

__global__ void binnedTimeIntegralB(double *time, double *out, double *sigmat, double *trig_cat, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, int N)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  int i;
  int n = N;
  
  if (N > 1000) 
    n = 1000;
  
  out[idx] = 0;
  
  for (i=0;i<n;i++) { 
    out[idx] += integral4piB(time[idx], A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0,phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigmat[i], trig_cat[i]) ;
  }
}

__global__ void binnedTimeIntegralBbar(double *time, double *out, double *sigmat, double *trig_cat, double A_0_mod, double A_S_mod, double A_pa_mod, double A_pe_mod, double G,double DG,double DM,double phi_0, double phi_S, double phi_pa, double phi_pe, double lambda_0_abs, double lambda_S_abs, double lambda_pa_abs, double lambda_pe_abs, int N)
{
 int idx = threadIdx.x + blockDim.x * blockIdx.x;
 int i;
 int n = N;
 
 if (N > 1000) 
    n = 1000;

 out[idx] = 0;
 
 for (i=0;i<n;i++) {
    out[idx] += integral4piBbar(time[idx], A_0_mod, A_S_mod, A_pa_mod, A_pe_mod, G, DG, DM, phi_0,phi_S, phi_pa, phi_pe, lambda_0_abs, lambda_S_abs, lambda_pa_abs, lambda_pe_abs, sigmat[i], trig_cat[i]);
 }
}

__global__ void DiffRate(double *data, 
                         double *out, 
                         double CSP,
                         double A_0_abs,
                         double A_S_abs,
                         double A_pa_abs,
                         double A_pe_abs,
                         double phis_0,
                         double phis_S,
                         double phis_pa,
                         double phis_pe,
                         double delta_S,
                         double delta_pa,
                         double delta_pe,
                         double lambda_0_abs,
                         double lambda_S_abs,
                         double lambda_pa_abs,
                         double lambda_pe_abs,
                         double G, 
                         double DG, 
                         double Dm, 
                         int Nevt)
{
int row = threadIdx.x + blockDim.x * blockIdx.x; //ntuple entry

if (row >= Nevt) { 
    return;
}

int i0 = row*10;// general rule for cuda matrices : index = col + row*N; as it is now, N = 10 (cthk,cthl,cphi,t,sigma_t,q_OS,q_SSK,eta_OS,eta_SSK,trigger cat)
int idx = 0 + i0; 
int idy = 1 + i0;
int idz = 2 + i0;
int idt = 3 + i0;
int idsigma_t = 4 + i0;
int idq_OS = 5 + i0;
int idq_SSK = 6 + i0;
int ideta_OS = 7 + i0;
int ideta_SSK = 8 + i0;
int itrig_cat = 9 + i0;

double helcosthetaK = data[idx];
double helcosthetaL = data[idy];
double helphi = data[idz];
double t = data[idt];
double sigma_t = data[idsigma_t];
double q_OS = data[idq_OS];
double q_SSK = data[idq_SSK];
double eta_OS = data[ideta_OS];
double eta_SSK = data[ideta_SSK];
int trig_cat = int(data[itrig_cat]);

double delta_0 = 0.;

double pdf_B = 0.;
double pdf_Bbar = 0.;  

double delta_t = delta_RunII(sigma_t);

double omega_OS = omega(eta_OS, 1);//0.353;
double omega_bar_OS = omega_bar(eta_OS, 1);//0.353;
double omega_SSK = omega(eta_SSK, 0);//0.259;
double omega_bar_SSK = omega_bar(eta_SSK, 0);//0.259;
        
// double exp_cosh_term = conv_cosh(t, G, DG, sigma_t);
// double exp_sinh_term = conv_sinh(t, G, DG, sigma_t);
// double exp_cos_term  = conv_exp_cos(t, G, Dm, sigma_t);
// double exp_sin_term  = conv_exp_sin(t, G, Dm, sigma_t);

double exp_cosh_term = conv_cosh_single(t, G, DG, delta_t);
double exp_sinh_term = conv_sinh_single(t, G, DG, delta_t);
double exp_cos_term  = conv_exp_cos_single(t, G, Dm, delta_t);
double exp_sin_term  = conv_exp_sin_single(t, G, Dm, delta_t);

for(int k = 1; k <= 10; k++){
    double Nk_term = Nk(A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,CSP,k);
    double fk_term = fk(helcosthetaK,helcosthetaL,helphi,k);
    
    double ak_term = ak(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cosh_term;
    double bk_term = bk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sinh_term;
    double ck_term = ck(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_cos_term;
    double dk_term = dk(phis_0,phis_S,phis_pa,phis_pe,delta_0,delta_S,delta_pa,delta_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,k)*exp_sin_term;
    
    double hk_B    = 3./(4.*M_PI)*(ak_term + bk_term + ck_term + dk_term);
    double hk_Bbar = 3./(4.*M_PI)*(ak_term + bk_term - ck_term - dk_term);
    
    pdf_B += Nk_term*hk_B*fk_term; 
             
    pdf_Bbar += Nk_term*hk_Bbar*fk_term;
    }

double integral[2] = {0.,0.};
// integral4pitime(integral, A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,G,DG,Dm,phis_0,phis_S,phis_pa,phis_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,sigma_t, trig_cat);
integral4pitime(integral, A_0_abs,A_S_abs,A_pa_abs,A_pe_abs,G,DG,Dm,phis_0,phis_S,phis_pa,phis_pe,lambda_0_abs,lambda_S_abs,lambda_pa_abs,lambda_pe_abs,delta_t, trig_cat);
    
double int_B = integral[0];
double int_Bbar = integral[1];

double time_acc = get_time_acc(t, trig_cat);

out[row] = time_acc*(1.+q_OS*(1.-2.*omega_OS))*(1.+q_SSK*(1.-2.*omega_SSK))
           *pdf_B/int_B
         + time_acc*(1.-q_OS*(1.-2.*omega_bar_OS))*(1.-q_SSK*(1.-2.*omega_bar_SSK))
           *pdf_Bbar/int_Bbar;
//            printf("omega_OS %lf omega_bar_OS %lf omega_SSK %lf omega_bar_SSK %lf\n q_OS %lf q_SSK %lf \n eta_OS %lf eta_SSK %lf \n", omega_OS, omega_bar_OS, omega_SSK, omega_bar_SSK, q_OS, q_SSK, eta_OS, eta_SSK);   
//            printf("omega_OS-omega_bar_OS %lf omega_SSK- omega_bar_SSK %lf\n diff_OS %lf diff_SSK %lf \n", omega_OS-omega_bar_OS, omega_SSK- omega_bar_SSK, 2*dp0_hf_OS-2*dp1_hf_OS*(eta_bar_OS-eta_OS), 2*dp0_hf_SSK-2*dp1_hf_SSK*(eta_bar_SSK-eta_SSK));         
}

