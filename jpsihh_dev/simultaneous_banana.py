from tools import initialize
initialize(0)
from bisect import *
from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
from iminuit import Minuit
import pycuda.gpuarray as gpuarray
import cPickle
from math import sqrt, isnan
from ROOT import TMath
from scipy import random as rnd
from scipy.special import erfinv
#from multiprocessing import Pool
from matplotlib import pyplot as plt
Gs = 0.6614 
DGs = 0.08543 
Gd = 1./ 1.519
Gddat = 1./1.520
Gudat = 1./1.638
Gu = 1./1.638
sf = 5367./5280
SAMPLE = "Bu"
#SW = "cor_sWeights_Bd"
SW = "truth_match"
if "dat" in SAMPLE: SW = "cor_sWeights_Bd"
dataDic = {}
k_TRUE = {"Bd":-Gd, "Bs": - Gs , "Bu": -Gu , "Bddat": -Gddat, "Budat": -Gudat}
dataDic["Bd"] = cPickle.load(file("/scratch28/diego/b2cc/Bd_MC_helcut_" + SW + "_0.3ps.crap"))
dataDic["Bu"] = cPickle.load(file("/scratch28/diego/b2cc/Bu_MC" + SW + "_0.3ps.crap"))
dataDic["Bs"] = cPickle.load(file("/scratch28/diego/b2cc/Bs_MC" + SW + "_0.3ps.crap"))

tmin = 1
tmax = 15.
BIN = 4
PTbins = [0, 3.8, 6, 9, 100]
PTmin = 0#1000*PTbins[BIN-1]
PTmax = 1e9#1000*PTbins[BIN]

#BREAK
delta = 1e-4
x = np.arange(-0.02, 0.02, delta)
y = np.arange(-0.02, 0.02, delta)

pairs = []
for thing in x:
    for crap in y: pairs.append([thing,crap])

X,Y = np.meshgrid(x,y)
def makeZ(zlist):
    Z = X*0
    k = 0
    for i in range (len(x)):
        for j in range( len(y)) :
            Z[j][i] = zlist[k]
            k += 1
    return Z

def do(SAMPLE):
    data = dataDic[SAMPLE]
    data.sort()
    data = np.float64(data)
    mask = (( data[:,0] > tmin ) * ( data[:,0] < tmax)*( data[:,6] > PTmin ) *( data[:,6] < PTmax ) )
    t_gpu = gpuarray.to_gpu(np.extract(mask,data[:,0]))
    mup_DZ_gpu = gpuarray.to_gpu(np.extract(mask, data[:,1]))
    mum_DZ_gpu = gpuarray.to_gpu(np.extract(mask,data[:,2]))
    hp_DZ_gpu = gpuarray.to_gpu(np.extract(mask,data[:,3]))
    hm_DZ_gpu = gpuarray.to_gpu(np.extract(mask, data[:,4]))


    G = k_TRUE[SAMPLE]
    sw_gpu = gpuarray.to_gpu(np.extract(mask, data[:,5]))
    expo_int = pycuda.cumath.exp(G*t_gpu)- np.exp(G*tmin)
    expo_int *= 1./(np.exp(G*tmax)-np.exp(G*tmin))

    Ntot = sum(mask)


    def FCN(epsmu, epsh):
        eff = (1. + epsmu*mup_DZ_gpu*mup_DZ_gpu) * (1. + epsmu*mum_DZ_gpu*mum_DZ_gpu)* (1. + epsh*hp_DZ_gpu*hp_DZ_gpu)* (1. + epsh*hm_DZ_gpu*hm_DZ_gpu)
        ws = 1./eff*sw_gpu
        cws =  gpuarray.to_gpu(np.cumsum(ws.get()))
        N0 = np.float64(cws[-1].get())
        cws *= 1./N0
        D = cws - expo_int
        Dmax= np.float64(gpuarray.max(pycuda.cumath.fabs(D)).get())
        KSstat = sqrt(Ntot)*Dmax
        Prob = TMath.KolmogorovProb(KSstat)
        if Prob < 1e-7: chi2 = 25 + KSstat 
        else: chi2 = 2*erfinv(1-Prob)**2

        return chi2
    def contFCN(pair): return FCN(pair[0],pair[1])
    
    chi2list = map(contFCN, pairs)
    Z = makeZ(chi2list)
    return Z


start = timer()
Z = do("Bu") #+ do("Bd") + do("Bs")
print "time:" , timer() - start
#BREAK
Zlist = list(Z)
v = []
for shit in Zlist:
    v.append(min(shit))
minchi2 = min(v)
print minchi2
if minchi2 > 4: print "look at minchi2"
cs = plt.contour(X,Y,Z, levels = [minchi2 +  2*1.15, minchi2 + 5.99,minchi2 + 11.8 ]) ## 4 is 1.5 sigma

plt.clabel(cs, inline=1, fontsize=10)
plt.xlabel("eps_mu")
plt.ylabel("eps_h")

plt.show()
