from tools import initialize
initialize()

from timeit import default_timer as timer
import numpy as np
import pycuda.cumath
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from iminuit import Minuit
import math
import pycuda.gpuarray as gpuarray
import cPickle
import os

sd = "float64"
dtype = getattr(np,sd) 

#Loading the signal=PDF!
mod = SourceModule(file(os.environ["IPAPATH"] + "/cuda/psIpatia.cu","r").read())
sig_pdf = mod.get_function("Ipatia")

#Loading the data
data = cPickle.load(file("/scratch15/diego/anaconda/exemplos/data_SnB.crap"))
#data = cPickle.load(file("data_SnB.ext"))
mydat = getattr(np,sd)(data[0])
Ndat = len(mydat)
massbins = getattr(np,sd)(data[1])
DM = np.float64(massbins[1] - massbins[0])
Mmax = max(massbins)
Mmin = min(massbins)

#Allocate memory for the data array
data_array_gpu = gpuarray.to_gpu(mydat)
bins_gpu = gpuarray.to_gpu(massbins)
#cudriver.memcpy_htod(bins_gpu, massbins)

tmp0 = np.empty_like(mydat)
tmp1 = np.empty_like(massbins)

sig_gpu = gpuarray.to_gpu(0*mydat)
int_gpu = gpuarray.to_gpu(0*massbins)
#BREAK
#Declare function to be passed by Minuit
def FCN(mu, sigma, l, beta, a, n, a2, n2, k, Ns, Nb):
    # Calculating integrals of signal(Ipatia) and background(Exp)
    sig_pdf(bins_gpu, int_gpu, np.float64(mu), np.float64(sigma), np.float64(l), np.float64(beta), np.float64(a), np.float64(n), np.float64(a2),np.float64(n2) , block = (1000,1,1), grid = (len(massbins)/1000,1,1))
    
    integral_ipa = np.sum((int_gpu).get())*DM
    
    if k!= 0 : 
        integral_exp = (np.exp(k*Mmax)-np.exp(k*Mmin))*1./k
    else : 
        integral_exp = (Mmax - Mmin)
        
    invint_b = 1./integral_exp
    invint_s = 1./integral_ipa
    Nexp = Ns+Nb
    fs = np.float64(Ns*1./Nexp)
    fb = np.float64(1.-fs)
    
    ### Evaluate the likelihood for each point
    ## sig_gpu : array with the signal likelihood, L_s^i
    ## bkg_gpu : array with the background likelihood, L_b^i
    sig_pdf(data_array_gpu,sig_gpu, np.float64(mu),np.float64(sigma),np.float64(l), np.float64(beta), np.float64(a), np.float64(n), np.float64(a2),np.float64(n2) , block = (512,1,1), grid = (len(mydat)/512 + 1,1,1))
    
    bkg_gpu = pycuda.cumath.exp(k*data_array_gpu)
    
    #Calculate total likelihood
    LL_gpu = pycuda.cumath.log(fs*invint_s*sig_gpu + fb*invint_b*bkg_gpu)
    extendLL =  Ndat*math.log(Nexp) -(Nexp)
    
    LL = np.float64(gpuarray.sum(LL_gpu).get()) + extendLL
    
    chi2 = -2*LL
    return chi2

#Initial time point
start = timer()

#Initialize fitter
fit = Minuit(FCN, mu = 5365., limit_mu = (5360., 5370.), sigma = 7., limit_sigma= (5.,9.), l = -3., limit_l= (-5.,-1.),beta = 0., limit_beta = (-1e-03,1e-03), a = 3., fix_a = True, n = 1,a2 = 6, limit_k = (-0.05,0), limit_Ns = (0.1*Ndat, 1.1*Ndat), limit_Nb = (0.1*Ndat,1.1*Ndat), Ns = 0.3*Ndat, Nb = 0.7*Ndat, fix_a2 = True, n2 = 1, fix_n = True, fix_n2 = True)
#Start minimization
fit.migrad()
fit.hesse()

print "GPU time:" , timer() - start
