from SympyBasic import *
from sympy import limit,oo,pprint, erf as Erf
Two = UraniaSR(2)
tp = USymbol("tp","\\frac{t}{\\sigma}", real = True)
t = USymbol("t","t", real = True)

w = USymbol("w","\\omega", positive = True)
G = USymbol("G","\\Gamma", positive =True)
s = USymbol("s","\\sigma", positive = True)
z = (-I*(tp-s*G)-w*s)/Sqrt(Two)
fade = Exp(-z*z)*(1+Erf(I*z))

expo = s*Sqrt(Pi/Two)*Exp(-t*G+G*s*s/Two)*(1+Erf((tp-G*s)/Sqrt(Two)))
print "Exponential:\n\n"
exponential_term = limit(expo,tp,oo)
pprint(exponential_term)

cost = (s*Sqrt(Pi/Two)*Exp(-tp**2/2)*re(fade)).subs(s*tp,t)
print "\n\nCosine Term:\n"
cosine_term = re(simplify(limit(cost,tp,oo)))
pprint(cosine_term)

sint = (-s*Sqrt(Pi/Two)*Exp(-tp**2/2)*im(fade)).subs(s*tp,t)

print "\n\nSine Term:\n"
sine_term = re(simplify(limit(sint,tp,oo)))
pprint(sine_term)

### Now let's save the expresion into cuda functions
from CppFunctionsMaker import FunctionPrinter
mysubs = [(s**2,Symbol("s2")),(G**2,Symbol("G2"))]
exp_cu = FunctionPrinter(exponential_term, (t,G,w,s),"exp_term")
exp_cu.sublist = mysubs
exp_cu.make()
cos_cu = FunctionPrinter(cosine_term, (t,G,w,s),"cos_term")
cos_cu.sublist = mysubs
cos_cu.make()
sin_cu = FunctionPrinter(sine_term, (t,G,w,s),"sin_term")
sin_cu.sublist = mysubs
sin_cu.make()
