from tools import initialize
initialize(0)

from tools import plt
import pycuda.cumath
from timeit import default_timer as timer
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from math import pi
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import Parameter, Free, Cat
import numpy as np
from PhisModel import mod, myModel
from phisParams import CSP
import cPickle
from math import pi

BLOCK_SIZE = 50 #(1 - 1024) For complex the max no. is smaller

#Calculate grid size
def getGrid(thiscat, BLOCK_SIZE):
    Nbunch = thiscat.Nevts *1. / BLOCK_SIZE
    if Nbunch > int(Nbunch): 
        Nbunch = int(Nbunch) +1
    else : 
        Nbunch = int(Nbunch)
    return  (Nbunch,1,1)
   
#Define common parameters
cats, Params = [], []
Params.append(Free("fL",0.5, limits=(0.3,.6)))
Params.append(Free("fpe",0.25, limits=(0.,0.3)))
Params.append(Free("phis",0.8, limits=(-1.,1.)))
Params.append(Free("dpa",0., limits=(-pi,pi)))
Params.append(Free("dpe",0., limits=(-pi,pi)))

Params.append(Free("G",0.6603, limits=(0.6,0.7)))
Params.append(Free("DG",0.0805, limits=(0.06,0.09)))
Params.append(Free("DM",17.7, limits=(16.,19.)))

#Get integral functions
integraB = mod.get_function("binnedTimeIntegralB")
integraBbar = mod.get_function("binnedTimeIntegralBbar")
              
for i in range(1,7):
    ibin = str(i)
    Bname = "B_"+ ibin
    Bbarname = "Bbar_"+ ibin
#    if i != 3: continue
    Params.append(Parameter("CSP_" + ibin ,CSP[i]))
    Params.append(Parameter("Fs_" + ibin,0., limits=(0.,1.)))
    Params.append(Parameter("ds_" + ibin,0., limits=(-10,10.)))

    ## Define category for i-th bin and flavour = B
    thiscat = Cat(Bname, "data_b_phi_0.5pi_"+ibin +".ext", getN = True) 
    
    thiscat.integra = integraB
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)

    ## Define category for i-th bin and flavour = Bbar
    thiscat = Cat(Bbarname, "data_bbar_phi_0.5pi_"+ibin +".ext", getN = True) 
    thiscat.integra = integraBbar
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
     
manager = myModel(Params, cats)
manager.createFit(errordef = 1)
start = timer()

manager.fit.migrad()
manager.fit.hesse()
#manager.fit.minos()
print timer() - start
manager.plotcat(cats[6])
plt.show()

#Multinest scan of the phase space
#manager.createMultinest("mnest_party")
#manager.mnest_vals()
