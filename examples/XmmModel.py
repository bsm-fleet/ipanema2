import numpy as np
import pycuda.cumath
import pycuda.driver as cudriver
import pycuda.gpuarray as gpuarray
from poisson_intervals import *
from toygen import poissonLL_b as poissonLL
from ModelBricks import ParamBox, cuRead
from tools import getSumLL_large, getSumLL_short, plt, plot1D


class XmmModel(ParamBox):
    def __init__(self, pars, cats):
        ParamBox.__init__(self, pars, cats)
