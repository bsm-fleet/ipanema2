from scipy import random as rnd
import numpy as np
from timeit import default_timer as timer
from tools import initialize
initialize(0)
import pycuda.gpuarray as gpuarray

import pycuda.curandom as curand
rand = curand.XORWOWRandomNumberGenerator()
poisson_gpu = rand.module.get_function("poisson_int")
M100 = np.int32(1e8)
BILLION = np.int32(1e9)


start = timer()

shit = rnd.poisson(2, M100)
print timer() - start
out_ = np.uint32(M100*[0])
start = timer()
out = gpuarray.to_gpu(out_)

rand.fill_poisson(out, np.float32(2.))
print timer() - start

###  BILLION
## out = gpuarray.to_gpu(np.uint32(BILLION*[0]))

## start = timer()

## shit = rnd.poisson(2, BILLION)
## print timer() - start

## start = timer()
## rand.fill_poisson(out, np.float32(2.))
## print timer() - start
